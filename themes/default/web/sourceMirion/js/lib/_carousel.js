/**********************************************/
/*
    * Слайдеры
*/
/**********************************************/

$(document).ready(function() {
    /* Главный слайд */
    if($('div').hasClass('slide-box-carousel')){
        var slide = $('.slide-box-carousel');

        slide.on('init reInit beforeChange', function(event, slick, currentSlide, nextSlide){
            var i = (currentSlide ? currentSlide : 0) + 1;
            // $('.slide-nav-counter').html('<span>' + ('0'+i).slice(-2) + '</span> / ' + ('0'+slick.slideCount).slice(-2));
            $('.slide-nav-counter').html('<span>' + (''+i).slice(-2) + '</span> / ' + (''+slick.slideCount).slice(-2));
        });

        slide.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            appendArrows: $('.slide-nav-arrows'),
            responsive: [
                /*{
                    breakpoint: 769,
                    settings: {
                       arrows: false,
                       dots: true,
                    }
                },*/
            ]
        });
    }

    $(window).resize(function () {
        directionsCarousel();
    });
    directionsCarousel();
    
    /* Карусель на главное ( Направления для мобильных только ) */
    function directionsCarousel() {
        if($('div').hasClass('direction-box-galleryXs')){
            var directionGallery = $('.direction-box-galleryXs');
            var innerWidth = window.innerWidth;
            if (innerWidth > 768) {
                if (directionGallery.hasClass('slick-initialized')) {
                    directionGallery.slick('unslick');
                }
            }
            else {
                if (!directionGallery.hasClass('slick-initialized')) {
                    directionGallery.slick({
                        fade: false,
                        infinite: false,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        autoplay: false,
                        autoplaySpeed: 5000,
                        dots: false,
                        arrows: true,
                        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33 2L3 32L33 62" stroke="#161616" stroke-width="3"/></svg></button>',
                        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L32 32L2 62" stroke="#161616" stroke-width="3"/></svg></button>',
                        responsive: [
                            {
                                breakpoint: 540,
                                settings: {
                                   slidesToShow: 1,
                                }
                            },
                        ]
                    });
                }
            }
        }
    }

    /* Карусель на главное ( Зеркала ) */
    if($('div').hasClass('mirrors-gallery-carousel')){
        var mirrorsGallery = $('.mirrors-gallery-carousel');

        mirrorsGallery.slick({
            fade: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            variableWidth: true,
            dots: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33 2L3 32L33 62" stroke="#161616" stroke-width="3"/></svg></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L32 32L2 62" stroke="#161616" stroke-width="3"/></svg></button>',
            /*responsive: [
                {
                    breakpoint: 860,
                    settings: {
                       arrows: true,
                       dots: true,
                       appendArrows: mirrorsGallery.parents('.js-slickGallery').find('.slickGallery-nav'),
                       appendDots: mirrorsGallery.parents('.js-slickGallery').find('.slickGallery-nav__dots')
                    }
                },
            ]*/
        });
    }

    /* Карусель сбоку ( о компании, подписаться ) */
    if($('div').hasClass('sideways-gallery-carousel')){
        var sidewaysGallery = $('.sideways-gallery-carousel');

        sidewaysGallery.slick({
            fade: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
        });
    }

    /* Карусель на главной ( Новости ) */
    if($('div').hasClass('news-box-carousel')){
        var newsGallery = $('.news-box-carousel');

        newsGallery.slick({
            fade: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33 2L3 32L33 62" stroke="#161616" stroke-width="3"/></svg></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L32 32L2 62" stroke="#161616" stroke-width="3"/></svg></button>',
            responsive: [
                /*{
                    breakpoint: 860,
                    settings: {
                       arrows: true,
                       dots: true,
                       appendArrows: newsGallery.parents('.js-slickGallery').find('.slickGallery-nav'),
                       appendDots: newsGallery.parents('.js-slickGallery').find('.slickGallery-nav__dots')
                    }
                },*/
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ]
        });
    }

    /* Карусель на главной ( Партнеры ) */
    if($('div').hasClass('partners-box-carousel')){
        var partnersGallery = $('.partners-box-carousel');

        partnersGallery.slick({
            fade: false,
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33 2L3 32L33 62" stroke="#161616" stroke-width="3"/></svg></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L32 32L2 62" stroke="#161616" stroke-width="3"/></svg></button>',
            responsive: [
                /*{
                    breakpoint: 860,
                    settings: {
                       arrows: true,
                       dots: true,
                       appendArrows: partnersGallery.parents('.js-slickGallery').find('.slickGallery-nav'),
                       appendDots: partnersGallery.parents('.js-slickGallery').find('.slickGallery-nav__dots')
                    }
                },*/
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                        variableWidth: false,
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 3,
                        variableWidth: false,
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                    }
                },
            ]
        });
    }

    /* Карусель на главной ( Отзывы ) */
    if($('div').hasClass('reviews-box-carousel')){
        var reviewsGallery = $('.reviews-box-carousel');

        reviewsGallery.on('init reInit beforeChange', function(event, slick, currentSlide, nextSlide){
            var i = (currentSlide ? currentSlide : 0) + 1;
            // $('.slide-nav-counter').html('<span>' + ('0'+i).slice(-2) + '</span> / ' + ('0'+slick.slideCount).slice(-2));
            $('.reviews-nav-counter').html('<span>' + ('0'+i).slice(-2) + '</span> ' + ('0'+slick.slideCount).slice(-2));
        });

        reviewsGallery.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            appendArrows: $('.reviews-nav-arrows'),
        });
    }

    /*
     * Портфолио
    */

    /* Главный слайд в шапке */
    if($('div').hasClass('portfolio-main-gallery')){
        var portfolioGal = $('.portfolio-main-gallery');

        portfolioGal.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                /*{
                    breakpoint: 769,
                    settings: {
                       arrows: false,
                       dots: true,
                    }
                },*/
            ]
        });
    }

    /* Карусель потрфолий в шапке */
    if($('div').hasClass('portfolio-caseOne-carousel')){
        var portfolioСard = $('.portfolio-caseOne-carousel');
        portfolioСard.each(function(){
            var elem = $(this);
            $(this).slick({
                fade: false,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 5000,
                dots: false,
                arrows: true,
                appendArrows: elem.parents('.js-portfolio-case').find('.portfolio-caseOne-arrows'),
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33 2L3 32L33 62" stroke="#161616" stroke-width="3"/></svg></button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L32 32L2 62" stroke="#161616" stroke-width="3"/></svg></button>',
                responsive: [
                    {
                        breakpoint: 540,
                        settings: {
                            slidesToShow: 1,
                            variableWidth: true,
                        }
                    },
                ]
            });
        })
    }

    /* Карусель во вьюхе потрфолии */
    if($('div').hasClass('portfolio-view-mainGallery')){
        var portfolioViewMain = $('.portfolio-view-mainGallery');
        portfolioViewMain.each(function(){
            $(this).slick({
                fade: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 5000,
                dots: false,
                arrows: true,
                responsive: [
                    /*{
                        breakpoint: 769,
                        settings: {
                           arrows: false,
                           dots: true,
                        }
                    },*/
                ]
            });
        })
    }

    portfolioCaseInit();

    /* Карусель во вьюхе потрфолии */
    if($('div').hasClass('portfolio-viewInline-carousel')){
        var portfolioviewInline = $('.portfolio-viewInline-carousel');
        portfolioviewInline.each(function(){
            $(this).slick({
                fade: false,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 5000,
                dots: false,
                arrows: true,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33 2L3 32L33 62" stroke="#161616" stroke-width="3"/></svg></button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg width="35" height="64" viewBox="0 0 35 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L32 32L2 62" stroke="#161616" stroke-width="3"/></svg></button>',
                responsive: [
                    {
                        breakpoint: 540,
                        settings: {
                            slidesToShow: 1,
                            variableWidth: true,
                        }
                    },
                ]
            });
        })
    }
});

function portfolioCaseInit() {
    /* Карусель во вьюхе потрфолии */
    if($('div').hasClass('portfolio-case-gallery')){
        var portfolioCase = $('.portfolio-case-gallery');
        portfolioCase.each(function(){
            $(this).slick({
                fade: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 5000,
                dots: false,
                arrows: true,
                responsive: [
                    /*{
                        breakpoint: 769,
                        settings: {
                           arrows: false,
                           dots: true,
                        }
                    },*/
                ]
            });
        })
    }
}