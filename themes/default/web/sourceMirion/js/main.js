$(document).ready(function() {
    var menumain = $('.header-bottom .menu .menu-main').html();
    $('.menu-fix .menu-mobile').append(menumain);

    /*
     * Menu при адаптации
    */
    // Кнопка открыть меню
    $('.menu-fix-icon').on('click', function(){
        if($(this).hasClass('active')){
            $('body').removeClass('bodymenu');
            $(this).removeClass('active');
            $(".menu-fix, .menu-fix__box, .menu-fix-icon-close").removeClass('active');
        } else {
            $('body').addClass('bodymenu');
            $(this).addClass('active');
            $(".menu-fix, .menu-fix__box, .menu-fix-icon-close").addClass('active');
        }

        return false;
    });
    // Кнопка закрыть меню
    $('.menu-fix').on('click', function(e){
        if($(e.target).hasClass('menu-fix-icon-close') || $(e.target).parents('.menu-fix-icon-close').length > 0 ){
            $('body').removeClass('bodymenu');
            $(".menu-fix-icon, .menu-fix-icon-close, .menu-fix, .menu-fix__box").removeClass('active');
            return false;
        }
    });

    
    /*
     * 
    */
    $(window).scroll(function(){
        
        fixMenu();

    });
    
    fixMenu()

    function fixMenu() {
        var $menu = $("header");
        if ($(this).scrollTop() <= 250){
            $menu.removeClass("header-fix");
        } else if($(this).scrollTop() > 250) {
            $menu.addClass("header-fix");
        }
    }
    


    Modernizr.on('webp', function (result) {
        if (result) {
            getBackLazy(true);
        }
        else {
            getBackLazy(false);
        }
    });
    
    function getBackLazy(webp) {
        $('.js-back-lazy').each(function(e){
            var img = $(this).data('img');
            if(webp){
                img = $(this).data('img-webp');
            }
            $(this).css({
                "background" : "url('"+img+"')"
            });
        });
    }

    /*
     * Действия при изменении окна 
    */
    $(window).resize(function () {
        getMoreReview();
    });

    /**********************************************/
    /*
     *** Кнопка больше информации СЕО-ТЕКСТ ***
    */
    $('.js-seo-txt-section-link').on('click', function(){
        var parent = $('.js-seo-txt-section');
        var txt = $(this).data('text');
        var txt_hidden = $(this).html();
        var height = parent.find('.js-seo-txt-more').height();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            parent.removeClass('active').attr('style', '');
            $(this).html(txt).data('text', txt_hidden);
        } else {
            $(this).addClass('active');
            parent.addClass('active').css('max-height', height);
            $(this).html(txt).data('text', txt_hidden);
        }
        return false;
    });
    /**********************************************/

    /*
     *  Рейтинг
    */
    // Клик по кнопке, чтобы показать всю таблицу
    $(document).delegate('.raiting-list-form .raiting-list__item', 'click', function(){
        var $this = $(this);
        var id = $this.data('id');
        $('.raiting-list-form .raiting-list__item').removeClass('active');
        $this.addClass('active').prevAll(".raiting-list__item").addClass('active');
        $this.parent().addClass('no-hover');
        $('#Review_rating').val(id);
        return false;
    });

    $('.portfolio-nav a').each(function() {
        if (location.protocol+"//"+location.hostname+'/'+$(this).attr('href') == window.location.href) {
            $(this).parent().addClass('active');
        }
        if (location.protocol+"//"+location.hostname+$(this).attr('href') == window.location.href) {
            $(this).parent().addClass('active');
        }
    });

    /*
     *  Отзывы
    */
    // фунция определения длины текста, если много добавляем кнопку
    function getMoreReview() {
        if($('div').hasClass('js-reviews-item')){
            $('.js-reviews-item').each(function () {
                var desc = $(this).find('.js-reviews-desc').height();
                var text = $(this).find('.js-reviews-text').height();
                if(text >= desc) {
                    $(this).find('.js-reviews-more').show();
                } else {
                    $(this).find('.js-reviews-more').hide();
                }
            });
        }
    }
    getMoreReview();

    // Клик по кнопке читать весь отзыв
    $(document).delegate('.js-reviews-more', 'click', function(){
        var modal = $('.js-reviewsModal');
        var content = $(this).parents('.js-reviews-item').html();
        modal.find('.reviews-box').html(content);
        modal.modal('show');
        return false;
    });

    /*
     * Портфолио
    */
    $(document).delegate('.js-zoom-case', 'click', function(){
        var innerWidth = window.innerWidth;
        if (innerWidth > 768) {
            var parent = $(this).parents('.js-portfolio-caseOne-item');
            var caseGallery = $(this).parents('.js-zoom-caseGallery');
            if(parent.hasClass('active')){
                parent.removeClass('active');
                $(this).parents('.js-zoom-caseGallery.slick-initialized').slick('refresh');
            } else {
                $('.js-portfolio-caseOne-item').removeClass('active');
                $('.js-zoom-caseGallery.slick-initialized').slick('refresh');

                parent.addClass('active');
                $(this).parents('.js-zoom-caseGallery.slick-initialized').slick('refresh');
            }
        }
        return false;
    });
});


// @prepros-append "./lib/modernizr-custom.js"
// @prepros-append "./lib/slick.js"
// @prepros-append "./lib/jquery.inputmask.js"
// @prepros-append "./lib/fancybox.js"
// @prepros-append "./lib/_form.js"
// @prepros-append "./lib/_carousel.js"