<?php $photos = $model->getAttributeValue($code)['gallery']; ?>
<?php if($photos) : ?>
	<?php foreach ($photos as $key => $photo): ?>
		<div class="sideways-box-back sideways-box-back-<?= $key; ?>">
            <picture>
	            <source media="(min-width: 1601px)" srcset="<?= $model->geFieldGalImageWebp(232, 330, false,  $photo['image']); ?>" type="image/webp">
	            <source media="(min-width: 1601px)" srcset="<?= $model->getFieldGalImageUrl(232, 330, false,  $photo['image']); ?>">

            	<source media="(min-width: 1101px)" srcset="<?= $model->geFieldGalImageWebp(170, 240, false,  $photo['image']); ?>" type="image/webp">
	            <source media="(min-width: 1101px)" srcset="<?= $model->getFieldGalImageUrl(170, 240, false,  $photo['image']); ?>">

            	<source media="(min-width: 1px)" srcset="<?= $model->geFieldGalImageWebp(1, 1, false,  $photo['image']); ?>" type="image/webp">
	            <source media="(min-width: 1px)" srcset="<?= $model->getFieldGalImageUrl(1, 1, false,  $photo['image']); ?>">

	            <img src="<?= $model->getFieldGalImageUrl(232, 330, false,  $photo['image']); ?>" alt="">
	        </picture>
		</div>
	<?php endforeach ?>
<?php endif; ?>