<?php $photos = $model->getAttributeValue($code)['gallery']; ?>
<?php if($photos) : ?>
	<div class="portfolio-viewInline-carousel carousel-gallery-arrowType <?= $class ?: ''; ?> slick-slider">
		<?php foreach ($photos as $key => $photo): ?>
			<div>
				<div class="portfolio-viewInline-carousel__item">
					<div class="portfolio-viewInline-carousel__img box-style-img">
						<?php if($fancybox) : ?>
							<a class="portfolio-viewInline-carousel__link fl fl-al-it-c fl-ju-co-c" data-fancybox="image" href="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>">
						<?php endif; ?>
			                    <picture>
						            <source media="(min-width: 1px)" srcset="<?= $model->geFieldGalImageWebp(0, 0, false,  $photo['image']); ?>" type="image/webp">
						            <source media="(min-width: 1px)" srcset="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>">

						            <img src="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>" alt="<?= $photo['title']; ?>">
						        </picture>
						<?php if($fancybox) : ?>
			                </a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif; ?>