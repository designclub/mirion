<?php $photos = $model->getAttributeValue($code)['gallery']; ?>
<?php if($photos) : ?>
	<div class="sideways-gallery sideways-gallery-carousel <?= $class ?: ''; ?> slick-slider">
		<?php foreach ($photos as $key => $photo): ?>
			<div>
				<div class="sideways-gallery__item">
					<div class="sideways-gallery__img box-style-img">
						<?php if($fancybox) : ?>
							<a class="sideways-gallery__link fl fl-al-it-c fl-ju-co-c" data-fancybox="image" href="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>">
						<?php endif; ?>
			                    <picture>
						            <source media="(min-width: 1px)" srcset="<?= $model->geFieldGalImageWebp(0, 0, false,  $photo['image']); ?>" type="image/webp">
						            <source media="(min-width: 1px)" srcset="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>">

					            	<source media="(min-width: 1px)" srcset="<?= $model->geFieldGalImageWebp(400, 300, false,  $photo['image']); ?>" type="image/webp">
						            <source media="(min-width: 1px)" srcset="<?= $model->getFieldGalImageUrl(400, 300, false,  $photo['image']); ?>">

						            <img src="<?= $model->getFieldGalImageUrl(0, 0, false,  $photo['image']); ?>" alt="<?= $photo['title']; ?>">
						        </picture>
						<?php if($fancybox) : ?>
			                </a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif; ?>