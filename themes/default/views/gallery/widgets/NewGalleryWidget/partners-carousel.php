<?php if($models->images) : ?>
	<div class="box-style">
		<div class="box-style__header">
			<div class="box-style__heading">
				<?= $models->name; ?>
			</div>
		</div>
	</div>
	<div class="partners-box partners-box-carousel carousel-gallery-arrowType slick-slider">
		<?php foreach ($models->images(['order' => 'position ASC']) as $key => $data): ?>
			<div>
				<div class="partners-box__item">
					<picture>
		                <!-- <source srcset="<?= $data->getImageUrlWebp(0, 0, false,null, 'file'); ?>" type="image/webp"> -->
		                <img src="<?= $data->getImageNewUrl(0, 0, false,null, 'file'); ?>" alt="<?= $data->alt; ?>" title="<?= $data->name; ?>">
		            </picture>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif; ?>