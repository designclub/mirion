<footer>
    <div class="footer">
        <div class="content fl fl-wr-w fl-ju-co-sp-b">
            <div class="footer__item footer__item_logo">
                <div class="footer-logo">
                    <?= CHtml::image($this->mainAssets . '/images/logo-white.svg', ''); ?>
                    <span>КОМПЛЕКСНОЕ <br>ОСНАЩЕНИЕ ИНТЕРЬЕРОВ</span>
                </div>
                <div class="footer-copy">
                    &copy; <?= date("Y"); ?> «MIRRION» <br>Все права защищены.
                </div>
            </div>
            <div class="footer__item footer__item_menu footer-menu">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'footermenu', 
                        'name' => 'nizhnee-menyu'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="footer__item footer__item_contact footer-contact fl fl-wr-w fl-ju-co-sp-b">
                <div class="footer-contact__item footer-contact__item_0">
                    <div class="footer-heading">Контакты</div>
                    <div class="footer-contact__mode">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 7
                        ]); ?>
                    </div>
                    <div class="footer-contact__phone">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 1
                        ]); ?>
                    </div>
                    <div class="footer-contact__email">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 2
                        ]); ?>
                    </div>
                    <div class="footer-contact__location">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 3
                        ]); ?>
                    </div>
                </div>
                <div class="footer-contact__item footer-contact__item_1">
                    <div class="footer-contact__soc">
                        <?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget', [
                            'id' => 2,
                            'view' => 'view-soc'
                        ]); ?>
                    </div>
                    <div class="footer-contact__but fl">
                        <a class="bt bt-white bt-animate-transform fl fl-al-it-c fl-ju-co-c" href="#">Магазин</a>
                    </div>
                    <div class="footer__dc-logo">
                        <a class="fl fl-wr-w fl-al-it-c" target="_blank" href="https://dcmedia.ru">
                            <span>Создано в</span>
                            <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/DCMedia-logo.svg');?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>