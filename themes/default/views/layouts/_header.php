<header id="header" class="<?= ($this->action->id=='index' && $this->id=='hp') ? 'header-home' : 'header-page'; ?>">
    <div class="header "> 
        <div class="content fl fl-al-it-c fl-ju-co-c">
            <div class="header__logo">
                <a class="fl fl-di-c fl-al-it-c fl-ju-co-c" href="/">
                    <?= CHtml::image($this->mainAssets . '/images/logo.svg', ''); ?>
                    <span>КОМПЛЕКСНОЕ ОСНАЩЕНИЕ ИНТЕРЬЕРОВ</span>
                </a>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="content fl fl-al-it-c fl-ju-co-sp-b">
            <div class="header-bottom__menu header-menu">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'main', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
                <div class="menu-fix-icon fl fl-al-it-c">
                    <div class="icon-bars fl fl-di-c fl-ju-co-sp-b">
                        <div class="icon-bars__item"></div>
                        <div class="icon-bars__item"></div>
                        <div class="icon-bars__item"></div>
                    </div>
                    <span>Меню</span>
                </div>
            </div>
            <div class="header-bottom__contact header-contact fl fl-al-it-c fl-ju-co-fl-e">
                <div class="header-contact__phone">
                    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                        'id' => 1
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</header>