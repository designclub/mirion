<div class="slide-box slide-box-carousel slick-slider">
	<?php foreach ($models as $key => $data): ?>
		<?php $big = 'image'; ?>
		<?php $xs = 'image'; ?>
		
		<?php if($data->image_big) : ?>
			<?php $big = 'image_big'; ?>
		<?php endif; ?>

		<?php if($data->image_xs) : ?>
			<?php $xs = 'image_xs'; ?>
		<?php endif; ?>

	    <div class="slide-box__item">
			<div class="slide-box__img box-style-img">
				<picture>
		            <!-- <source media="(min-width: 1921px)" srcset="<?= $data->getImageUrlWebp(0, 0, true, null, $big); ?>" type="image/webp"> -->
		            <source media="(min-width: 1921px)" srcset="<?= $data->getImageNewUrl(0, 0, true, null, $big); ?>">

	            	<!-- <source media="(min-width: 641px)" srcset="<?= $data->getImageUrlWebp(0, 0, true, null, 'image'); ?>" type="image/webp"> -->
		            <source media="(min-width: 641px)" srcset="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>">

	            	<!-- <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(640, 400, false, null, $xs); ?>" type="image/webp"> -->
		            <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(640, 400, false, null, $xs); ?>">

		            <!-- <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(400, 370, false, null, $xs); ?>" type="image/webp"> -->
		            <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(400, 370, false, null, $xs); ?>">
					<!-- , Yii::app()->getModule('slider')->uploadPath . "/mobile"
					, Yii::app()->getModule('slider')->uploadPath . "/mobile" -->

		            <img src="<?= $data->getImageNewUrl(0, 0, true, null, 'image'); ?>" alt="">
		        </picture>
			</div>
			<div class="slide-box__content">
				<div class="slide-box__info">
					<div class="slide-box__name">
						<?= $data->name; ?>
					</div>
					<?php /*if($data->button_link && $data->button_name) : ?>
						<div class="slide-box__but fl">
							<a class="but but-white but-svg but-svg-right" href="<?= $data->button_link; ?>">
								<span><?= $data->button_name; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif;*/ ?>
				</div>
			</div>
	    </div>
	<?php endforeach ?>
</div>

<div class="slide-nav-content fl fl-al-it-c">
	<div class="slide-nav-content__item fl fl-al-it-c fl-ju-co-sp-b">
		<a class="but but-white but-svg but-svg-right" href="/portfolio">
			<span>Посмотреть портфолио</span>
			<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
		</a>
	</div>
	<div class="slide-nav-content__item slide-nav fl fl-al-it-c fl-ju-co-fl-e">
		<div class="slide-nav__progress"></div>
		<div class="slide-nav__counter slide-nav-counter nav-counter"></div>
		<div class="slide-nav__arrows slide-nav-arrows nav-arrows fl fl-al-it-c"></div>
	</div>
</div>