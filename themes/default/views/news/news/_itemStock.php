<div class="stock-box__item">
    <a class="stock-box__link" href="<?= Yii::app()->createUrl('/news/news/viewStock', ['slug' => $data->slug]); ?>">
        <div class="stock-box__img">
            <picture class="">
                <!-- <source srcset="<?= $data->getImageUrlWebp(480, 277, true, null,'image'); ?>" type="image/webp"> -->
                <img src="<?= $data->getImageNewUrl(480, 277, true, null,'image'); ?>" alt="">
            </picture>
        </div>
    </a>
</div>