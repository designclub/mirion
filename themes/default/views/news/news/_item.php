<div class="news-box__item">
    <div class="news-box__img box-style-img">
        <a href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
            <picture>
                <!-- <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(378, 286, true, null, 'image'); ?>" type="image/webp"> -->
                <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(378, 286, true, null, 'image'); ?>">

                <!-- <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(260, 175, true, null, 'image'); ?>" type="image/webp"> -->
                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(260, 175, true, null, 'image'); ?>">

                <img src="<?= $data->getImageNewUrl(378,286, true, null, 'image'); ?>" alt="<?= CHtml::encode($data->title); ?>">
            </picture>
        </a>
    </div>
    <div class="news-box__info">
        <div class="news-box__date"><?= date("d.m.Y", strtotime($data->date)); ?></div>
        <div class="news-box__name">
            <a class="news-box__link" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
                <?= CHtml::encode($data->title); ?>
            </a>
        </div>
        <div class="news-box__but fl">
            <a class="but but-svg but-svg-right" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
                <span>Читать новость</span>
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
            </a>
        </div>
    </div>
</div>