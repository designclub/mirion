<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     https://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = $model->meta_title ?: $model->title;
$this->description = $model->meta_description;
$this->keywords = $model->meta_keywords;
?>

<?php
$this->breadcrumbs = [
    $model->category->name => ['/news/news/index'],
    $model->title
];
?>

<div class="page-content news-page-content">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>
        <h1><?= CHtml::encode($model->title); ?></h1>
        <div class="news-view fl fl-wr-w fl-ju-co-sp-b">
            <div class="news-view__info">
                <div class="news-view__date">Дата публикации: <?= date("d.m.Y", strtotime($model->date)); ?></div>
                <?= $model->full_text; ?>
            </div>
            <div class="news-view__sidebar news-sidebar">
                <div class="news-sidebar__header"><?= Yii::app()->getModule('news')->newsHeadSidebar; ?></div>
                <?php $this->widget('application.modules.news.widgets.ReadAlsoNewsWidget', [
                    'id' => $model->id
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->renderPartial('//page/page/_navPage', [
    'model' => $model,
    'prevText' => 'Предыдущая новость',
    'nextText' => 'Следующая новость',
]) ?>