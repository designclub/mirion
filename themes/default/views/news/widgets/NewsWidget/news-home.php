<?php if($models) : ?>
	<div class="news-box news-box-carousel carousel-gallery-arrowType slick-slider">
		<?php foreach ($models as $key => $data) : ?>
			<div>
				<?php $this->render('../../news/_item', ['data' => $data]); ?>
			</div>
        <?php endforeach; ?>
	</div>
<?php endif; ?>