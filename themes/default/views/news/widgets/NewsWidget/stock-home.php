<?php if($models) : ?>
	<div class="stock-home section-home">
		<div class="box-style">
			<div class="box-style__header fl fl-wr-w fl-al-it-c">
                <div class="box-style__heading">
                    <?= $category->name_short; ?>
                </div>
                <div class="box-style__but">
			    	<a class="but-link but-link-green but-link-svg but-link-svg-right" href="<?= Yii::app()->createUrl('/news/news/indexStock'); ?>">
			    		<span>Все акции</span>
			    		<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/right-arrow.svg'); ?>
			    	</a>
			    </div>
            </div>
		</div>
	    <div class="stock-box stock-box-carousel slick-slider">
	        <?php foreach ($models as $key => $data) : ?>
	        	<div class="slick-item">
	            	<?php $this->render('../../news/_itemStock', ['data' => $data]); ?>
	        	</div>
	        <?php endforeach; ?>
	    </div>
	</div>
<?php endif; ?>