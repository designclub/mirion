<?php if($models) : ?>
	<div class="news-box">
		<?php foreach ($models as $key => $data) : ?>
			<?php $this->render('../../news/_item', ['data' => $data]); ?>
        <?php endforeach; ?>
	</div>
<?php endif; ?>