<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<?php $parentPageTitle = $model->parentPage->title_short ?>

<div class="page-content portfolio-page-typeTwo">
	<div class="content">
		<?php $this->widget('application.components.MyTbBreadcrumbs', [
	        'links' => $this->breadcrumbs,
	    ]); ?>

	    <div class="js-pagination-top">
			<h1><?= $model->title; ?></h1>

			<?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
				'id' => $model->id,
				'limit' => 5,
				'listView' => true,
				'view' => 'portfolio-caseOne-list'
			]); ?>
		</div>
	</div>
	<div class="portfolio-inline-back"></div>
	<div class="back-img back-logo back-logo-white back-t-r"></div>
</div>

<?php Yii::app()->getClientScript()->registerScript("js-portfolio-typeTwo", "
    if(!$('div').hasClass('pagination-box')){
    	$('.portfolio-inline-back').addClass('portfolio-inline-back-2');
    }
"); ?>