<div class="navPage-section">
	<?php $prevPage = $model->prevPage(); ?>
	<?php $nextPage = $model->nextPage(); ?>

	<?php if(!$prevText) : ?>
		<?php $prevText = strip_tags(($prevPage->title_short) ?: $prevPage->title, '<br>'); ?>
	<?php endif; ?>

	<?php if(!$nextText) : ?>
		<?php $nextText = strip_tags(($nextPage->title_short) ?: $nextPage->title, '<br>'); ?>
	<?php endif; ?>

	<?php if(!empty($prevPage) && !empty($nextPage)) : ?>
		<div class="content fl fl-al-it-c">
			<div class="navPage-section__item fl fl-al-it-c">
				<a class="navPage-section__link fl fl-al-it-c" href="<?= $prevPage->getUrl(); ?>">
					<span class="navPage-section__span">
						<span class="navPage-section__name"><?= $prevText; ?></span>
					</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/icon-prev.svg'); ?>
				</a>
			</div>
			<div class="navPage-section__item fl fl-di-c fl-ju-co-c">
				<a class="navPage-section__link fl fl-al-it-c" href="<?= $nextPage->getUrl(); ?>">
					<span class="navPage-section__span">
						<span class="navPage-section__name"><?= $nextText; ?></span>
					</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/icon-next.svg'); ?>
				</a>
			</div>
		</div>
	<?php endif; ?>
</div>