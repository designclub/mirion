<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content page-company-content">
    <div class="content">
    	<?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>
   </div>
    <div class="company-section">
    	<?php if(!$model->getAttributeValue('company-box1') && !$model->getAttributeValue('company-box2') && !$model->getAttributeValue('company-box3') && !$model->getAttributeValue('company-box4')) : ?>
    		<h1><?= $model->title; ?></h1>
			<?= $model->body; ?>
    	<?php endif; ?>

    	<?php if($model->getAttributeValue('company-box1')) : ?>
	    	<div class="company-boxOne sideways-box sideways-box-l">
				<div class="content fl fl-wr-w fl-al-it-c">
					<div class="sideways-box__info company-boxOne__info fl fl-di-c fl-ju-co-c">
						 <div class="box-style">
							<div class="box-style__header">
								<h1 class="box-style__heading">
									<?= $model->getAttributeValue('company-box1')['name']; ?>
								</h1>
							</div>
						</div>
						<div class="sideways-box__desc company-boxOne__desc">
							<?= $model->getAttributeValue('company-box1')['value']; ?>
						</div>
					</div>
					<div class="sideways-box__media company-boxOne__media">
						<div class="sideways-box__gallery company-boxOne__gallery">
							<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
								'id' => $model->id,
								'code' => 'company-box1',
								'view' => 'sideways-gallery-home'
							]); ?>
						</div>
					</div>
				</div>
				<div class="sideways-box-back sideways-box-back-t-r">
					<picture>
			            <source media="(min-width: 1601px)" srcset="<?= $model->geFieldImageWebp(232, 330, false, $model->getAttributeValue('company-box1')['image']); ?>" type="image/webp">
			            <source media="(min-width: 1601px)" srcset="<?= $model->getFieldImageUrl(232, 330, false, $model->getAttributeValue('company-box1')['image']); ?>">

		            	<source media="(min-width: 1101px)" srcset="<?= $model->geFieldImageWebp(170, 240, false, $model->getAttributeValue('company-box1')['image']); ?>" type="image/webp">
			            <source media="(min-width: 1101px)" srcset="<?= $model->getFieldImageUrl(170, 240, false, $model->getAttributeValue('company-box1')['image']); ?>">

		            	<source media="(min-width: 1px)" srcset="<?= $model->geFieldImageWebp(1, 1, false, $model->getAttributeValue('company-box1')['image']); ?>" type="image/webp">
			            <source media="(min-width: 1px)" srcset="<?= $model->getFieldImageUrl(1, 1, false, $model->getAttributeValue('company-box1')['image']); ?>">

			            <img src="<?= $model->getFieldImageUrl(232, 330, false, $model->getAttributeValue('company-box1')['image']); ?>" alt="">
			        </picture>
				</div>
		    </div>
	    <?php endif; ?>

	    <?php if($model->getAttributeValue('company-box2')) : ?>
		    <div class="company-boxTwo sideways-box sideways-box-l">
				<div class="content fl fl-wr-w fl-al-it-c">
					<div class="sideways-box__info company-boxTwo__info fl fl-di-c fl-ju-co-c">
						 <div class="box-style">
							<div class="box-style__header">
								<div class="box-style__heading">
									<?= $model->getAttributeValue('company-box2')['name']; ?>
								</div>
							</div>
						</div>
						<div class="sideways-box__desc company-boxTwo__desc">
							<?= $model->getAttributeValue('company-box2')['value']; ?>
						</div>
					</div>
					<div class="sideways-box__media company-boxTwo__media fl fl-al-it-fl-e fl-ju-co-fl-s">
						<div class="sideways-box__gallery company-boxTwo__img">
							<picture>
					            <source media="(min-width: 1px)" srcset="<?= $model->geFieldImageWebp(0, 0, false, $model->getAttributeValue('company-box2')['image']); ?>" type="image/webp">
					            <source media="(min-width: 1px)" srcset="<?= $model->getFieldImageUrl(0, 0, false, $model->getAttributeValue('company-box2')['image']); ?>">

					            <img src="<?= $model->getFieldImageUrl(0, 0, false,  $model->getAttributeValue('company-box2')['image']); ?>" alt="">
					        </picture>
						</div>
					</div>
				</div>
				<div class="back-img back-logo back-logo-black back-b-r"></div>
		    </div>
	   	<?php endif; ?>

	   	<?php if($model->getAttributeValue('company-box3')) : ?>
		    <div class="company-boxThree sideways-box">
				<div class="content fl fl-wr-w fl-al-it-c">
					<div class="sideways-box__info company-boxThree__info fl fl-di-c fl-ju-co-c">
						<?php 
							$box3ButName = $model->getAttributeValue('company-box3')['butName'];
							$box3ButLink = $model->getAttributeValue('company-box3')['butLink'];
						 ?>
						<div class="box-style">
							<div class="box-style__header">
								<div class="box-style__heading">
									<?= $model->getAttributeValue('company-box3')['name']; ?>
								</div>
							</div>
						</div>
						<div class="sideways-box__desc company-boxThree__desc">
							<?= $model->getAttributeValue('company-box3')['value']; ?>
						</div>
						<?php if($box3ButLink) : ?>
							<div class="sideways-box__but company-boxThree__but fl fl-ju-co-fl-e">
								<a class="but but-svg but-svg-right" href="<?= $box3ButLink; ?>">
									<span><?= ($box3ButName) ?: 'Портфолио'; ?></span>
									<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
								</a>
							</div>
						<?php endif; ?>
					</div>
					<div class="sideways-box__media company-boxThree__media">
						<div class="sideways-box__gallery company-boxThree__gallery">
							<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
								'id' => $model->id,
								'code' => 'company-box3',
								'view' => 'sideways-gallery-home'
							]); ?>
						</div>
					</div>
				</div>
				<div class="back-img back-logo back-logo-white back-b-l"></div>
			</div>
		<?php endif; ?>

		<?php if($model->getAttributeValue('company-box4')) : ?>
			<div class="company-boxFour sideways-box">
				<div class="content fl fl-wr-w fl-al-it-c">
					<div class="sideways-box__info company-boxFour__info fl fl-di-c fl-ju-co-c">
						<?php 
							$box4ButName = $model->getAttributeValue('company-box4')['butName'];
							$box4ButLink = $model->getAttributeValue('company-box4')['butLink'];
						 ?>
						 <div class="box-style">
							<div class="box-style__header">
								<div class="box-style__heading">
									<?= $model->getAttributeValue('company-box4')['name']; ?>
								</div>
							</div>
						</div>
						<div class="sideways-box__desc company-boxFour__desc">
							<?= $model->getAttributeValue('company-box4')['value']; ?>
						</div>
						<?php if($box4ButLink) : ?>
							<div class="sideways-box__but company-boxFour__but fl fl-ju-co-fl-e">
								<a class="but but-svg but-svg-right" href="<?= $box4ButLink; ?>">
									<span><?= ($box4ButName) ?: 'Магазин'; ?></span>
									<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
								</a>
							</div>
						<?php endif; ?>
					</div>
					<div class="sideways-box__media company-boxFour__media fl fl-al-it-fl-e fl-ju-co-fl-s">
						<div class="sideways-box__gallery company-boxFour__img">
							<picture>
					            <source media="(min-width: 1px)" srcset="<?= $model->geFieldImageWebp(0, 0, false, $model->getAttributeValue('company-box4')['image']); ?>" type="image/webp">
					            <source media="(min-width: 1px)" srcset="<?= $model->getFieldImageUrl(0, 0, false, $model->getAttributeValue('company-box4')['image']); ?>">

					            <img src="<?= $model->getFieldImageUrl(0, 0, false,  $model->getAttributeValue('company-box4')['image']); ?>" alt="">
					        </picture>
						</div>
					</div>
				</div>
				<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
					'id' => $model->id,
					'code' => 'company-box4-back',
					'view' => 'back-img'
				]); ?>
		    </div>
		<?php endif; ?>
    </div>
</div>
