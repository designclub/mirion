<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-content portfolio-view-typeOne">
    <div class="content">
    	<?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>
		<h1><?= $model->title; ?></h1>
		<div class="portfolio-view-header">
			<div class="content fl fl-wr-w fl-al-it-c">
				<div class="portfolio-view-header__info">
					<?= $model->body; ?>
				</div>
				<div class="portfolio-view-header__media">
					<?php $photos = $model->photos(['order' => 'photos.position ASC']); ?>
					<?php if($photos) : ?>
						<div class="portfolio-view-mainGallery slick-slider">
							<?php foreach ($photos as $key => $photo) : ?>
								<div class="portfolio-view-mainGallery__item">
									<div class="portfolio-view-mainGallery__img box-style-img">
										<picture>
								            <!-- <source media="(min-width: 1px)" srcset="<?= $photo->getImageUrlWebp(0, 0, false,  'image'); ?>" type="image/webp"> -->
								            <source media="(min-width: 1px)" srcset="<?= $photo->getImageNewUrl(0, 0, false,  'image'); ?>">

								            <img src="<?= $photo->getImageNewUrl(0, 0, false, 'image'); ?>" alt="">
								        </picture>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<?php if(!$model->getAttributeValue('box1') && !$model->getAttributeValue('box2') && !$model->getAttributeValue('box3') && !$model->getAttributeValue('box4') && !$model->getAttributeValue('box5')) : ?>
		<div class="page-content">
		</div>
	<?php endif; ?>

	<?php if($model->getAttributeValue('box1')) : ?>
		<div class="portfolio-viewBox portfolio-viewBox1 sideways-box">
			<div class="content fl fl-wr-w">
				<div class="sideways-box__info portfolio-viewBox__info fl fl-di-c fl-ju-co-fl-s">
					<?php 
						$box1ButName = $model->getAttributeValue('box1')['butName'];
						$box1ButLink = $model->getAttributeValue('box1')['butLink'];
					 ?>
					<div class="box-style">
						<div class="box-style__header">
							<div class="box-style__heading">
								<?= $model->getAttributeValue('box1')['name']; ?>
							</div>
						</div>
					</div>
					<div class="sideways-box__desc portfolio-viewBox__desc">
						<?= $model->getAttributeValue('box1')['value']; ?>
					</div>
					<?php if($box1ButLink) : ?>
						<div class="sideways-box__but portfolio-viewBox__but fl fl-ju-co-fl-e">
							<a class="but but-svg but-svg-right" href="<?= $box1ButLink; ?>">
								<span><?= $box1ButName; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="sideways-box__media portfolio-viewBox__media">
					<div class="sideways-box__gallery portfolio-viewBox__gallery">
						<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
							'id' => $model->id,
							'code' => 'box1',
							'view' => 'sideways-gallery-home'
						]); ?>
					</div>
				</div>
			</div>
			<div class="back-img back-logo back-logo-white back-b-l"></div>
		</div>
	<?php endif; ?>

	<?php if($model->getAttributeValue('box2')) : ?>
		<div class="portfolio-viewBox portfolio-viewBox2 sideways-box sideways-box-l">
			<div class="content fl fl-wr-w fl-al-it-c">
				<div class="sideways-box__info portfolio-viewBox__info fl fl-di-c fl-ju-co-c">
					<?php 
						$box2ButName = $model->getAttributeValue('box2')['butName'];
						$box2ButLink = $model->getAttributeValue('box2')['butLink'];
					 ?>
					<div class="box-style">
						<div class="box-style__header">
							<div class="box-style__heading">
								<?= $model->getAttributeValue('box2')['name']; ?>
							</div>
						</div>
					</div>
					<div class="sideways-box__desc portfolio-viewBox__desc">
						<?= $model->getAttributeValue('box2')['value']; ?>
					</div>
					<?php if($box2ButLink) : ?>
						<div class="sideways-box__but portfolio-viewBox__but fl fl-ju-co-fl-e">
							<a class="but but-svg but-svg-right" href="<?= $box2ButLink; ?>">
								<span><?= $box2ButName; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="sideways-box__media portfolio-viewBox__media">
					<div class="sideways-box__gallery portfolio-viewBox__gallery">
						<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
							'id' => $model->id,
							'code' => 'box2',
							'view' => 'sideways-gallery-home'
						]); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if($model->getAttributeValue('box3')) : ?>
		<div class="portfolio-viewBox portfolio-viewBox3 sideways-box">
			<div class="content fl fl-wr-w">
				<div class="sideways-box__info portfolio-viewBox__info fl fl-di-c fl-ju-co-fl-s">
					<?php 
						$box3ButName = $model->getAttributeValue('box3')['butName'];
						$box3ButLink = $model->getAttributeValue('box3')['butLink'];
					 ?>
					<div class="box-style">
						<div class="box-style__header">
							<div class="box-style__heading">
								<?= $model->getAttributeValue('box3')['name']; ?>
							</div>
						</div>
					</div>
					<div class="sideways-box__desc portfolio-viewBox__desc">
						<?= $model->getAttributeValue('box3')['value']; ?>
					</div>
					<?php if($box3ButLink) : ?>
						<div class="sideways-box__but portfolio-viewBox__but fl fl-ju-co-fl-e">
							<a class="but but-svg but-svg-right" href="<?= $box3ButLink; ?>">
								<span><?= $box3ButName; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="sideways-box__media portfolio-viewBox__media">
					<div class="sideways-box__gallery portfolio-viewBox__gallery">
						<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
							'id' => $model->id,
							'code' => 'box3',
							'view' => 'sideways-gallery-home'
						]); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if($model->getAttributeValue('box4')) : ?>
		<div class="portfolio-viewBox portfolio-viewBox4 sideways-box sideways-box-l">
			<div class="content fl fl-wr-w">
				<div class="sideways-box__info portfolio-viewBox__info fl fl-di-c fl-ju-co-fl-s">
					<?php 
						$box4ButName = $model->getAttributeValue('box4')['butName'];
						$box4ButLink = $model->getAttributeValue('box4')['butLink'];
					 ?>
					<div class="box-style">
						<div class="box-style__header">
							<div class="box-style__heading">
								<?= $model->getAttributeValue('box4')['name']; ?>
							</div>
						</div>
					</div>
					<div class="sideways-box__desc portfolio-viewBox__desc">
						<?= $model->getAttributeValue('box4')['value']; ?>
					</div>
					<?php if($box4ButLink) : ?>
						<div class="sideways-box__but portfolio-viewBox__but fl fl-ju-co-fl-e">
							<a class="but but-svg but-svg-right" href="<?= $box4ButLink; ?>">
								<span><?= $box4ButName; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="sideways-box__media portfolio-viewBox__media fl fl-al-it-fl-e fl-ju-co-fl-s">
					<div class="sideways-box__gallery portfolio-viewBox__img">
						<picture>
				            <source media="(min-width: 1px)" srcset="<?= $model->geFieldImageWebp(0, 0, false, $model->getAttributeValue('box4')['image']); ?>" type="image/webp">
				            <source media="(min-width: 1px)" srcset="<?= $model->getFieldImageUrl(0, 0, false, $model->getAttributeValue('box4')['image']); ?>">

				            <img src="<?= $model->getFieldImageUrl(0, 0, false,  $model->getAttributeValue('box4')['image']); ?>" alt="">
				        </picture>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if($model->getAttributeValue('box5')) : ?>
		<div class="portfolio-viewInline portfolio-viewInline5">
			<div class="content">
				<div class="portfolio-viewInline__info">
					<?php 
						$box5ButName = $model->getAttributeValue('box5')['butName'];
						$box5ButLink = $model->getAttributeValue('box5')['butLink'];
					 ?>
					<div class="box-style">
						<div class="box-style__header">
							<div class="box-style__heading">
								<?= $model->getAttributeValue('box5')['name']; ?>
							</div>
						</div>
					</div>
					<div class="portfolio-viewInline__desc">
						<?= $model->getAttributeValue('box5')['value']; ?>
					</div>
					<?php if($box5ButLink) : ?>
						<div class="portfolio-viewInline__but fl fl-ju-co-fl-e">
							<a class="but but-svg but-svg-right" href="<?= $box5ButLink; ?>">
								<span><?= $box5ButName; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<div class="portfolio-viewInline__media">
					<div class="portfolio-viewInline__gallery">
						<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
							'id' => $model->id,
							'fancybox' => true,
							'code' => 'box5',
							'view' => 'portfolio-viewInline-carousel'
						]); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
