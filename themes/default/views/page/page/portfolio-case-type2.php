<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<?php $parentPageTitle = $model->parentPage->title_short ?>

<div class="home-section">
	<div class="slider-home">
		<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
			'category_id' => 1
		]); ?>
	</div>
</div>

<div class="page-content portfolio-page-typeTwo">
	<div class="content">
		<?php $this->widget('application.components.MyTbBreadcrumbs', [
	        'links' => $this->breadcrumbs,
	    ]); ?>

	    <div class="js-pagination-top">
			<h1><?= $model->title; ?></h1>

			<?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
				'id' => $model->id,
				'limit' => 4,
				'listView' => true,
				'view' => 'portfolio-caseTwo-list'
			]); ?>
		</div>
	</div>
</div>

<?php Yii::app()->getClientScript()->registerScript("js-portfolio-typeTwo", "
    if(!$('div').hasClass('pagination-box')){
    	$('.portfolio-inline-back').addClass('portfolio-inline-back-2');
    }
    /*$(document).delegate('.js-zoom-case', 'click', function(){
    	var parent = $(this).parents('.js-portfolio-caseOne-item');
    	var caseGallery = $(this).parents('.js-zoom-caseGallery');
    	if(parent.hasClass('active')){
    		// parent.addClass('loading');
			parent.removeClass('active');
			caseGallery.slick('refresh');
			setTimeout(function(){
				// parent.removeClass('loading');
			}, 100);
		} else {
			// parent.addClass('loading');
			$('.js-portfolio-caseOne-item').removeClass('active');
			$('.js-zoom-caseGallery').slick('refresh');

			parent.addClass('active');
			caseGallery.slick('refresh');
			setTimeout(function(){
				// parent.removeClass('loading');
			}, 100);
		}
	    return false;
    });*/
    
"); ?>