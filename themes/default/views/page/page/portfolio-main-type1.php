<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<?php $parentPageTitle = $model->parentPage->title_short ?>

<div class="page-content portfolio-page-typeOne">
	<div class="portfolio-header">
		<div class="content fl fl-wr-w">
			<div class="portfolio-header__info">
		    	<?php $this->widget('application.components.MyTbBreadcrumbs', [
		            'links' => $this->breadcrumbs,
		        ]); ?>
				<div>
					<h1 class="portfolio-header__name">
						<strong><?= $parentPageTitle; ?></strong> <?= $model->title_short; ?> <?= $model->svg_icon; ?>
					</h1>
					<div class="portfolio-header__desc">
						<?= $model->name_desc; ?>
					</div>
				</div>
				<?= $model->body_short; ?>
			</div>
			<div class="portfolio-header__media">
				<?php $photos = $model->photos(['order' => 'photos.position ASC']); ?>
				<?php if($photos) : ?>
					<div class="portfolio-main-gallery slick-slider">
						<?php foreach ($photos as $key => $photo) : ?>
							<div class="portfolio-main-gallery__item">
								<div class="portfolio-main-gallery__img box-style-img">
									<picture>
							            <!-- <source media="(min-width: 1px)" srcset="<?= $photo->getImageUrlWebp(0, 0, false,  'image'); ?>" type="image/webp"> -->
							            <source media="(min-width: 1px)" srcset="<?= $photo->getImageNewUrl(0, 0, false,  'image'); ?>">

							            <img src="<?= $photo->getImageNewUrl(0, 0, false, 'image'); ?>" alt="">
							        </picture>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="back-img back-logo back-logo-black back-b-l"></div>
	</div>

	<?php $this->widget('application.modules.page.widgets.DirectionsWidget', [
		'id' => 4,
		'limit' => 5,
		'view' => 'direction-nav-widget'
	]); ?>

	<?php $childPages = $model->childPages(['order' => 'childPages.order ASC']); ?>
	<?php if($childPages) : ?>
		<div class="portfolio-inline">
			<div class="content">
				<div class="portfolio-inline__item">
					<div class="portfolio-case-section js-portfolio-case">
						<div class="portfolio-case-section__item portfolio-case-section__item-grid">
							<div class="portfolio-caseOne-box portfolio-caseOne-grid">
								<?php foreach ($childPages as $key => $page) : ?>
									<?php if($page->status == Page::STATUS_PUBLISHED): ?>
										<div class="portfolio-caseOne-box__item portfolio-caseOne-grid__item">
											<div class="portfolio-caseOne-box__img box-style-img">
												<a href="<?= $page->getUrl(); ?>">
													<picture>
														<!-- <source media="(min-width: 1px)" srcset="<?= $page->getImageUrlWebp(585, 400, true,  'image'); ?>" type="image/webp"> -->
														<source media="(min-width: 1px)" srcset="<?= $page->getImageNewUrl(585, 400, true,  'image'); ?>">

														<img src="<?= $page->getImageNewUrl(585, 400, true, 'image'); ?>" alt="">
													</picture>
												</a>
											</div>
											<div class="portfolio-caseOne-box__info">
												<a class="portfolio-caseOne-box__link" href="<?= $page->getUrl(); ?>"><?= $page->title; ?></a>
											</div>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="portfolio-case-section__arrows portfolio-caseOne-arrows carousel-gallery-arrowType"></div>
					</div>
				</div>
			</div>
			<div class="portfolio-inline-back"></div>
		</div>
	<?php else : ?>
		<div class="page-content">
			<div class="content">
				<?= $model->body; ?>
			</div>
		</div>
	<?php endif; ?>

	<?php $this->renderPartial('_navPage', ['model' => $model]) ?>
</div>
