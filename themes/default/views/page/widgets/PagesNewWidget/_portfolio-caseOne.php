<div class="portfolio-caseOne-list__item js-portfolio-caseOne-item fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
	<?php $photos = $data->photos(['order' => 'photos.position ASC']); ?>
	<?php if($photos) : ?>
		<div class="portfolio-caseOne-list__media">
			<div class="portfolio-case-gallery js-zoom-caseGallery slick-slider">
				<?php foreach ($photos as $key => $photo) : ?>
					<div class="portfolio-case-gallery__item js-zoom-case">
						<div class="portfolio-case-gallery__img box-style-img">
							<picture>
					            <!-- <source media="(min-width: 401px)" srcset="<?= $photo->getImageUrlWebp(1200, 700, true, null, 'image'); ?>" type="image/webp"> -->
					            <source media="(min-width: 401px)" srcset="<?= $photo->getImageNewUrl(1200, 700, true, null, 'image'); ?>">

					            <!-- <source media="(min-width: 1px)" srcset="<?= $photo->getImageUrlWebp(360, 230, true, null, 'image'); ?>" type="image/webp"> -->
		            			<source media="(min-width: 1px)" srcset="<?= $photo->getImageNewUrl(360, 230, true, null, 'image'); ?>">

					            <img src="<?= $photo->getImageNewUrl(1200, 700, true, null, 'image'); ?>" alt="">
					        </picture>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php else : ?>
		<div class="portfolio-caseOne-list__media js-zoom-caseGallery">
			<div class="portfolio-caseOne-list__img js-zoom-case box-style-img">
				<picture>
		            <!-- <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(1200, 700, true, null, 'image'); ?>" type="image/webp"> -->
		            <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(1200, 700, true, null, 'image'); ?>">

	            	<!-- <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(360, 230, true, null, 'image'); ?>" type="image/webp"> -->
		            <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(360, 230, true, null, 'image'); ?>">

		            <img src="<?= $data->getImageNewUrl(1200, 700, true, null, 'image'); ?>" alt="">
		        </picture>
			</div>
		</div>
	<?php endif; ?>
	<div class="portfolio-caseOne-list__info">
		<a href="<?= $data->getUrl(); ?>">
			<div class="portfolio-caseOne-list__name">
				<?= $data->title_short; ?>
			</div>
		</a>
		<div class="portfolio-caseOne-list__desc">
			<?= $data->body_short; ?>
		</div>
		<div class="portfolio-caseOne-list__but fl">
			<a class="but but-svg but-svg-right" href="<?= $data->getUrl(); ?>">
				<span>Подробнее</span>
				<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
			</a>
		</div>
	</div>
</div>