<?php if(!empty($pages) && $dataProvider->itemCount): ?>
	<?php  $this->widget(
        'bootstrap.widgets.TbListView',
        [
            'dataProvider' => $dataProvider,
            'id' => 'portfolio-caseOne-list',
            'itemView' => '_portfolio-caseOne',
            'template'=>'
                {items}
                {pager}
            ',
            'itemsCssClass' => "portfolio-caseOne-list",
            'htmlOptions' => [
                // 'class' => 'portfolio-caseOne-list'
            ],
            'ajaxUpdate'=>true,
            'enableHistory' => false,
            // 'ajaxUrl'=>'GET',
            'pagerCssClass' => 'pagination-box',
                'pager' => [
                'header' => '',
                'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                // 'lastPageLabel'  => false,
                // 'firstPageLabel' => false,
                'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                'maxButtonCount' => 5,
                'htmlOptions' => [
                    'class' => 'pagination'
                ],
            ],
            'beforeAjaxUpdate' => "js:function(id, options){
                $('.preloader').addClass('active');
            }",
            'afterAjaxUpdate' => "js:function(){
                var top = $('.js-pagination-top').offset().top;
                $('body,html').animate({
                    scrollTop: top + 'px'
                }, 400);
                portfolioCaseInit();
                $('.preloader').removeClass('active');
            }"
        ]
    ); ?>
<?php endif; ?>
