<?php Yii::import('application.modules.menu.components.YMenu'); ?>
<?php $this->widget(
     'zii.widgets.CMenu',
     [
         'encodeLabel' => false,
         'items' => $siteMapArray,
         'htmlOptions' => [
            'class' => 'sitemap-menu',
         ],
     ]
 ); ?>

<style>

</style>