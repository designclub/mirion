<?php if($models) : ?>
	<?php $childPages = $models->childPages(['condition' => 'status = :status', 'params' => ['status' => Page::STATUS_PUBLISHED], 'order' => 'childPages.order ASC', 'limit' => $limit]); ?>
	<?php if($childPages) : ?>
		<div class="direction-box carousel-gallery-arrowType fl fl-wr-w ">
			<?php foreach ($childPages as $key => $data) : ?>
				<?php $this->render('_item-direction', [
					'models' => $models,
					'data' => $data
				]); ?>
			<?php endforeach; ?>
			<div class="direction-box__item direction-box__item_all fl fl-al-it-c fl-ju-co-c">
				<div class="direction-box__but fl">
					<a class="but but-white but-svg but-svg-right" href="<?= $models->getUrl(); ?>">
						<span>Все направления</span>
						<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
					</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>