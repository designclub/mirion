<?php if($models) : ?>
	<?php $childPages = $models->childPages(['condition' => 'status = :status', 'params' => ['status' => Page::STATUS_PUBLISHED],'order' => 'childPages.order ASC']); ?>
	<?php if($childPages) : ?>
		<div class="direction-box direction-box-page fl fl-wr-w ">
			<?php foreach ($childPages as $key => $data) : ?>
				<?php $this->render('_item-direction', [
					'models' => $models,
					'data' => $data
				]); ?>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
<?php endif; ?>