<a href="<?= $data->getUrl(); ?>" class="direction-box__item">
	<div class="direction-box__info">
		<div class="direction-box__header fl fl-di-c fl-ju-co-sp-b">
			<div>
				<div class="direction-box__name">
					<strong><?= $models->title_short; ?></strong> <?= $data->title_short; ?> <?= $data->svg_icon; ?>
				</div>
				<div class="direction-box__desc">
					<?= $data->name_desc; ?>
				</div>
			</div>
			<div class="direction-box__but fl">
				<div class="but but-svg but-svg-right">
					<span>Подробнее</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="direction-box__img box-style-img">
		<picture>
            <!-- <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(380,380, false, null, 'icon'); ?>" type="image/webp"> -->
            <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(380,380, false, null, 'icon'); ?>">

        	<!-- <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(260, 230, false, null, 'icon'); ?>" type="image/webp"> -->
            <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(260, 230, false, null, 'icon'); ?>">

            <img src="<?= $data->getImageNewUrl(380, 380, true, null, 'icon'); ?>" alt="">
        </picture>
	</div>
</a>
