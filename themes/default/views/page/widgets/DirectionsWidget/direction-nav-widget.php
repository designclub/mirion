<?php if($models) : ?>
	<?php $childPages = $models->childPages(['condition' => 'status = :status', 'params' => ['status' => Page::STATUS_PUBLISHED],'order' => 'childPages.order ASC', 'limit' => $limit]); ?>
	<?php if($childPages) : ?>
		<div class="portfolio-nav">
			<div class="content fl fl-wr-w fl-juc-co-sp-b">
				<?php foreach ($childPages as $key => $data) : ?>
					<div class="portfolio-nav__item fl fl-al-it-c fl-ju-co-c">
						<a class="portfolio-nav__link" href="<?= $data->getUrl(); ?>">
							<?= $data->title_short; ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>