<div id="subcribeModal" class="modal modal-my fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Подпишитесь <br>на рассылку
                    </div>
                    <div class="box-style__desc">
                        и получайте только самое лучшее <br>за неделю
                    </div>
                </div>
            </div>

            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                'id'          => 'subscribe-form-modal-widget',
                'htmlOptions' => [
                    'class' => 'subscribe-form',
                    'data-type' => 'ajax-form'
                ],
            ]); ?>
            <?php if (Yii::app()->user->hasFlash('subscribe-success')): ?>
                <div class="message-success modal-subscribe-message">
                    <?= Yii::app()->user->getFlash('subscribe-success') ?>
                </div>
                <script>
                    $('#subcribeModal').modal('hide');
                    $('#subcribeMessageModal').modal('show');
                    setTimeout(function(){
                        $('#subcribeMessageModal').modal('hide');
                    }, 5000);
                </script>
            <?php endif ?>
             <div class="modal-body">
                 <?= $form->textFieldGroup($model, 'name', [
                    'widgetOptions'=>[
                        'htmlOptions' => [
                            'class' => '',
                            'autocomplete' => 'off',
                            'placeholder' => 'Ваше имя'
                        ],
                    ],
                ]); ?>

                <?= $form->textFieldGroup($model, 'email', [
                    'widgetOptions'=>[
                        'htmlOptions' => [
                            'class' => '',
                            'autocomplete' => 'off',
                            'required' => 'true',
                            'placeholder' => 'Ваш E-mail'
                        ],
                    ],
                ]); ?>
                
                <?= $form->hiddenField($model, 'verify'); ?>

                <?php $model->check = 1; ?>
                <div class="checkbox-box form-group <?= $model->hasErrors('check') ? 'has-error' : ''; ?>">
                    <?= $form->checkbox($model, 'check');?>
                    <label for="<?= get_class($model).'_check'; ?>">
                        Согласен с <a target="_blank" href="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 4]); ?>"> Условиями обработки персональных данных </a>
                    </label>
                    <?= $form->error($model, 'check');?>
                </div>

                <div class="form-bot">
                    <div class="form-button">
                        <div class="form-captcha">
                            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>"></div>
                            <?= $form->error($model, 'verifyCode');?>
                        </div>
                    </div>
                    <div class="form-button">
                        <button data-send="ajax" type="submit" class="bt bt-animate-transform">
                            <span>Подписаться</span>
                        </button>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<?php Yii::app()->clientScript->registerScript($this->id.'-script', "
    $('#subcribeModal').on('show.bs.modal', function (e) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        $.getScript('https://www.google.com/recaptcha/api.js', function () {});
        head.appendChild(script);
    });
") ?>