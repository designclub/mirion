<div class="subscribe-section fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
    <div class="subscribe-section__header">
        <div class="subscribe-section__heading">
            <?php 
                $titleSub = Yii::t('DCMedia', 'Want to get the best articles from DCMedia once a week?');
                $titleSub = explode(' ', $titleSub);
            foreach ($titleSub as $key => $item) : ?>
                <span class="back-color-white"><?= $item; ?></span>
            <?php endforeach; ?>
        </div>
        <div class="subscribe-section__desc">
            <?= Yii::t('DCMedia', 'I rassylki get only the best for the week') ?>
        </div>
    </div>


    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
        'id'          => 'subscribe-form-widget',
        'htmlOptions' => [
            'class'     => 'subscribe-section__form subscribe-form fl fl-wr-w fl-ju-co-sp-b',
            'data-typeForm' => 'ajax-form'
        ],
    ]); ?>
        
        <?php if (Yii::app()->user->hasFlash('subscribe-success')): ?>
            <script>
                $('#subcribeMessageModal').modal('show');
                setTimeout(function(){
                    $('#subcribeMessageModal').modal('hide');
                }, 5000);
            </script>
        <?php endif ?>
        
        <?= $form->hiddenField($model, 'verify'); ?>
        
        <div class="subscribe-form__email">
            <?= $form->textFieldGroup($model, 'email', [
                'widgetOptions'=>[
                    'htmlOptions' => [
                        'class' => '',
                        'autocomplete' => 'off',
                        'required' => 'true',
                        'placeholder' => Yii::t('DCMedia', 'Your E-mail*')
                    ],
                ],
            ]); ?>
        </div>
        
        <div class="subscribe-form__check checkbox-box">
            <div class="checkbox-box__item">
                <div class="form-group <?= $model->hasErrors('check') ? 'has-error' : ''; ?>">
                    <?= $form->checkbox($model, 'check', ['checked'=>'checked']);?>
                    <?= $form->labelEx($model, 'check');?>
                    <?= $form->error($model, 'check');?>
                </div>
            </div>
        </div>
        <div class="subscribe-form__but">
            <button type="submit" class="but but-noborder subscribe-form-button" data-sender="ajax">
                <span><?= Yii::t('DCMedia', 'Subscribe'); ?></span>
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/right-arrow.svg'); ?>
            </button>
        </div>
    <?php $this->endWidget(); ?>
</div>