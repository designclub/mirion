<div class="reviews-box__item  js-reviews-item">
    <div class="reviews-box__header fl fl-wr-w fl-al-it-c">
        <div class="reviews-box__img fl fl-wr-w fl-al-it-c fl-ju-co-c">
            <picture>
                <!-- <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(94, 94, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>" type="image/webp"> -->
                <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(94, 94, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>">
                
                <!-- <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(56, 56, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>" type="image/webp"> -->
                <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(56, 56, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>">

                <img src="<?= $data->getImageNewUrl(94, 94, true, Yii::app()->getTheme()->getAssetsUrl() . '/images/reviews-nophoto.png', 'image'); ?>" alt="">
            </picture>
        </div>
        <div class="reviews-box__info">
            <div class="reviews-box__raiting reviews-raiting">
                <div class="raiting-list">
                    <?php for ($i=1; $i <= 5; $i++) : ?>
                        <div class="raiting-list__item <?= ($i <= $data->rating) ? 'active' : ''; ?>"></div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="reviews-box__name">
                <span><?= CHtml::encode( $data->username); ?></span>
                <?= CHtml::encode( $data->name_desc); ?>
            </div>
        </div>
    </div>
    <div class="reviews-box__description js-reviews-desc">
        <div class="reviews-box__text js-reviews-text">
            <?= $data->text; ?>
        </div>
    </div>
    <?php if($is_more) : ?>
        <div class="reviews-box__more js-reviews-more">
            <a class="reviews-box__link js-reviews-link" href="#">
                <span>Читать весь отзыв</span>
            </a>
        </div>
    <?php endif; ?>
</div>
