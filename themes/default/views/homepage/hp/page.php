<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title ?: $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;

?>

<div class="home-section">
	<div class="slider-home">
		<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
			'category_id' => 1
		]); ?>
	</div>

	<div class="direction-home js-back-lazy" 
		data-img="<?= $page->getFieldImageUrl(0, 0, false,  $page->getAttributeValue('directions')['image']); ?>" 
		data-img-webp="<?= $page->geFieldImageWebp(0, 0, false,  $page->getAttributeValue('directions')['image']); ?>"
		>
		<div class="content">
			<div class="box-style">
				<div class="box-style__header">
					<div class="box-style__heading">
						<?= $page->getAttributeValue('directions')['name']; ?>
					</div>
				</div>
			</div>

			<?php $this->widget('application.modules.page.widgets.DirectionsWidget', [
				'id' => 4,
				'limit' => 5,
				'view' => 'direction-widget'
			]); ?>
		</div>
		<div class="mirrors-gallery-home hidden">
			<div class="content">
				<?php 
					$mirrorGalButName = $page->getAttributeValue('mirror-gallery')['butName'];
					$mirrorGalButLink = $page->getAttributeValue('mirror-gallery')['butLink'];
				 ?>
				<div class="box-style <?= ($mirrorGalButLink) ? 'box-style-but' : ''; ?>">
					<div class="box-style__header fl fl-ju-sp-b">
						<div class="box-style__heading">
							<?= $page->getAttributeValue('mirror-gallery')['name']; ?>
						</div>
						<?php if($mirrorGalButLink) : ?>
							<div class="box-style__but fl">
								<a class="but but-svg but-svg-right" href="<?= $mirrorGalButLink; ?>">
									<span><?= ($mirrorGalButName) ?: 'Магазин'; ?></span>
									<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
					'id' => $page->id,
					'fancybox' => true,
					'code' => 'mirror-gallery',
					'view' => 'mirrors-gallery-home'
				]); ?>
			</div>	
		</div>
	</div>

	<div class="company-home sideways-box sideways-box-l">
		<div class="content fl fl-wr-w fl-al-co-c">
			<div class="sideways-box__info company-home__info fl fl-di-c fl-ju-co-c">
				<?php 
					$companyButName = $page->getAttributeValue('company')['butName'];
					$companyButLink = $page->getAttributeValue('company')['butLink'];
				 ?>
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading">
							<?= $page->getAttributeValue('company')['name']; ?>
						</div>
					</div>
				</div>
				<div class="sideways-box__desc company-home__desc">
					<?= $page->getAttributeValue('company')['value']; ?>
				</div>
				<?php if($companyButLink) : ?>
					<div class="sideways-box__but company-home__but fl">
						<a class="but but-white but-svg but-svg-right" href="<?= $companyButLink; ?>">
							<span><?= ($companyButName) ?: 'О компании'; ?></span>
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="sideways-box__media company-home__media">
				<div class="sideways-box__gallery company-home__gallery">
					<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
						'id' => $page->id,
						'code' => 'company',
						'view' => 'sideways-gallery-home'
					]); ?>
				</div>
			</div>
		</div>
		<div class="back-img back-logo back-logo-black back-t-r"></div>
	</div>

	<div class="news-home">
		<div class="content">
			<?php 
				$newsButName = $page->getAttributeValue('news')['butName'];
				$newsButLink = $page->getAttributeValue('news')['butLink'];
			 ?>
			<div class="box-style <?= ($newsButLink) ? 'box-style-but' : ''; ?>">
				<div class="box-style__header fl fl-ju-sp-b">
					<div class="box-style__heading">
						<?= $page->getAttributeValue('news')['name']; ?>
					</div>
					<?php if($newsButLink) : ?>
						<div class="box-style__but fl">
							<a class="but but-svg but-svg-right" href="<?= $newsButLink; ?>">
								<span><?= ($newsButName) ?: 'Все новости'; ?></span>
								<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php $this->widget('application.modules.news.widgets.NewsWidget', [
				'categories' => Yii::app()->getModule('news')->newsId,
				'limit' => 7,
				'view' => 'news-home'
			]); ?>
		</div>
	</div>

	<div class="subcribe-home sideways-box">
		<div class="content fl fl-wr-w fl-al-co-c">
			<div class="sideways-box__info subcribe-home__info fl fl-di-c fl-ju-co-c">
				<?php 
					$subcribeButName = $page->getAttributeValue('subcribe')['butName'];
					$subcribeButLink = $page->getAttributeValue('subcribe')['butLink'];
				 ?>
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading">
							<?= $page->getAttributeValue('subcribe')['name']; ?>
						</div>
					</div>
				</div>
				<div class="sideways-box__desc subcribe-home__desc">
					<?= $page->getAttributeValue('subcribe')['value']; ?>
				</div>
				<?php if($subcribeButLink) : ?>
					<div class="sideways-box__but subcribe-home__but fl">
						<a class="but but-white but-svg but-svg-right" data-toggle="modal" data-target="#subcribeModal" href="#">
							<span><?= ($subcribeButName) ?: 'Подписаться'; ?></span>
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/icon-arrow.svg'); ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="sideways-box__media subcribe-home__media">
				<div class="sideways-box__gallery subcribe-home__gallery">
					<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
						'id' => $page->id,
						'code' => 'subcribe',
						'view' => 'sideways-gallery-home'
					]); ?>
				</div>
			</div>
		</div>
		<div class="back-img back-img-1 back-b-l"></div>
	</div>

	<div class="partners-home">
		<div class="content">
			<?php $this->widget('application.modules.gallery.widgets.NewGalleryWidget', [
				'id' => 1,
				'view' => 'partners-carousel'
			]); ?>
		</div>
	</div>

	<div class="reviews-home sideways-box sideways-box-l">
		<div class="content fl fl-wr-w fl-al-co-c">
			<div class="sideways-box__info reviews-home__info fl fl-di-c fl-ju-co-c">
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading">
							<?= $page->getAttributeValue('reviews')['name']; ?>
						</div>
					</div>
				</div>
				<?php $this->widget('application.modules.review.widgets.ReviewCarouselWidget', [
					'view' => 'review-home-carousel'
				]); ?>
			</div>
			<div class="sideways-box__media reviews-home__media">
				<div class="sideways-box__gallery reviews-home__gallery">
					<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
						'id' => $page->id,
						'code' => 'reviews',
						'view' => 'sideways-gallery-home'
					]); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="questions-home sideways-box">
		<div class="content fl fl-wr-w fl-al-co-c">
			<div class="sideways-box__info questions-home__info fl fl-di-c fl-ju-co-c">
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading">
							<?= $page->getAttributeValue('questions')['name']; ?>
						</div>
					</div>
				</div>
				<div class="sideways-box__desc questions-home__desc">
					<?= $page->getAttributeValue('questions')['value']; ?>
				</div>
			</div>
			<div class="sideways-box__media questions-home__media">
				<div class="sideways-box__gallery questions-home__gallery">
					<?php $this->widget('application.modules.gallery.widgets.CustomFieldWidget', [
						'id' => $page->id,
						'code' => 'questions',
						'view' => 'sideways-gallery-home'
					]); ?>
				</div>
			</div>
		</div>
		<div class="back-img back-logo back-logo-white back-b-l"></div>
	</div>
</div>