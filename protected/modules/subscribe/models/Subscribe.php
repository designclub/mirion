<?php

/**
 * This is the model class for table "{{subscribe}}".
 *
 * The followings are the available columns in table '{{subscribe}}':
 * @property integer $id
 * @property string $email
 * @property integer $status
 * @property string $create_time
 */
class Subscribe extends yupe\models\YModel
{
	public $verify;
	public $check;
	public $verifyCode;

	const STATUS_NOT_ACTIVE = 0; // активен
	const STATUS_ACTIVE     = 1;     // не активен
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subscribe}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['email', 'required'],
			['email', 'length', 'max' => 50],
			['status', 'numerical', 'integerOnly'=>true],
			['name', 'length', 'max'=>255],
			['email', 'unique', 'message' => Yii::t('SubscribeModule.subscribe', 'Email already busy')],
			['email', 'email'],
    		// ['email', 'checkEmail'],
    		['check', 'compare', 'compareValue' => 1, 'message' => 'Для продолжения вы должны согласиться на обработку персональных данных'],
			['name, create_time, verify', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, name, email, status, create_time', 'safe', 'on'=>'search'],
		];
	}

	public function behaviors()
	{
		return [
			'CTimestampBehavior' => [
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'setUpdateOnCreate' => true,
				'updateAttribute' => null,
			],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'          => 'ID',
			'name'        => 'Имя',
			'email'       => 'Email',
			'status'      => 'Статус подписчика',
			'create_time' => 'Дата создания',
			'verifyCode'  => 'Код проверки',
			'check'       => 'Я даю согласие на обработку персональных данных',
		];
	}

	public function checkEmail($attribute, $params)
    {
        $model = Subscribe::model()->find('email = :email', [':email' => $this->$attribute]);

        if ($model) {
            $this->addError('email', Yii::t('SubscribeModule.subscribe', 'Email already busy'));
        }
    }

	public function beforeValidate(){
        if (isset($_POST['g-recaptcha-response'])) {
	        if ($_POST['g-recaptcha-response']=='') {
	            $this->addError('verifyCode', 'Пройдите проверку reCAPTCHA..');
	        } else {
	            $ip = CHttpRequest::getUserHostAddress();
	            $post = [
	                'secret' => Yii::app()->params['secretkey'],
	                'response' => $_POST['g-recaptcha-response'],
	                'remoteip' => $ip,
	            ];

	            $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	            $response = curl_exec($ch);
	            curl_close($ch);

	            $response = CJSON::decode($response);
	            if (isset($response['success']) and isset($response['error-codes']) and $response['success']===false) {
	                $this->addError('verifyCode', implode(', ', $response['error-codes']));
	            }
	        }
	    }
        return parent::beforeValidate();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getStatusList()
	{
		return [
			self::STATUS_NOT_ACTIVE => 'не активен',
			self::STATUS_ACTIVE     => 'активен',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return $data[$this->status];
	}

	public function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->status = self::STATUS_ACTIVE;
		}
		return parent::beforeSave();
	}

	public function afterSave()
	{
		$to = Yii::app()->getModule('subscribe')->email_to;
        $from = Yii::app()->getModule('subscribe')->email_from;

        $theme = "Уведомление о новом подписчике";

        $to = explode(',', $to);
        
        $body = "<table><tr><td>Имя: </td><td>{$this->name}</td></tr><tr><td>E-mail: </td><td>{$this->email}</td></tr></table>";

        foreach ($to as $email) {
            Yii::app()->mail->send($from, $email, $theme, $body);
        }

		return parent::afterSave();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subscribe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function emptyOnInvalid($attribute, $params)
    {
        if ($this->hasErrors()) {
            $this->verifyCode = null;
        }
    }
}
