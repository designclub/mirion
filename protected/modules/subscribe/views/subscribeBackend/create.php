<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('SubscribeModule.subscribe', 'Рассылки') => ['/subscribe/subscribeBackend/index'],
    Yii::t('SubscribeModule.subscribe', 'Добавление'),
];

$this->pageTitle = Yii::t('SubscribeModule.subscribe', 'Рассылки - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('SubscribeModule.subscribe', 'Управление Рассылками'), 'url' => ['/subscribe/subscribeBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('SubscribeModule.subscribe', 'Добавить Рассылку'), 'url' => ['/subscribe/subscribeBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('SubscribeModule.subscribe', 'Рассылки'); ?>
        <small><?=  Yii::t('SubscribeModule.subscribe', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>