<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('SubscribeModule.subscribe', 'Рассылки') => ['/subscribe/subscribeBackend/index'],
    $model->id,
];

$this->pageTitle = Yii::t('SubscribeModule.subscribe', 'Рассылки - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('SubscribeModule.subscribe', 'Управление Рассылками'), 'url' => ['/subscribe/subscribeBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('SubscribeModule.subscribe', 'Добавить Рассылку'), 'url' => ['/subscribe/subscribeBackend/create']],
    ['label' => Yii::t('SubscribeModule.subscribe', 'Рассылка') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('SubscribeModule.subscribe', 'Редактирование Рассылки'), 'url' => [
        '/subscribe/subscribeBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('SubscribeModule.subscribe', 'Просмотреть Рассылку'), 'url' => [
        '/subscribe/subscribeBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('SubscribeModule.subscribe', 'Удалить Рассылку'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/subscribe/subscribeBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('SubscribeModule.subscribe', 'Вы уверены, что хотите удалить Рассылку?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('SubscribeModule.subscribe', 'Просмотр') . ' ' . Yii::t('SubscribeModule.subscribe', 'Рассылки'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'email',
        'status',
        'create_time',
    ],
]); ?>
