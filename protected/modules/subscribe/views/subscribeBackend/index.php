<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('SubscribeModule.subscribe', 'Рассылки') => ['/subscribe/subscribeBackend/index'],
    Yii::t('SubscribeModule.subscribe', 'Управление'),
];

$this->pageTitle = Yii::t('SubscribeModule.subscribe', 'Рассылки - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('SubscribeModule.subscribe', 'Управление Рассылками'), 'url' => ['/subscribe/subscribeBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('SubscribeModule.subscribe', 'Добавить Рассылку'), 'url' => ['/subscribe/subscribeBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('SubscribeModule.subscribe', 'Рассылки'); ?>
        <small><?=  Yii::t('SubscribeModule.subscribe', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('SubscribeModule.subscribe', 'Поиск Рассылок');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('subscribe-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('SubscribeModule.subscribe', 'В данном разделе представлены средства управления Рассылками'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'subscribe-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            'name',
            'email',
            'status',
            'create_time',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
