<?php
/**
* Отображение для subscribe/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     http://yupe.ru
**/
$this->pageTitle = Yii::t('SubscribeModule.subscribe', 'subscribe');
$this->description = Yii::t('SubscribeModule.subscribe', 'subscribe');
$this->keywords = Yii::t('SubscribeModule.subscribe', 'subscribe');

$this->breadcrumbs = [Yii::t('SubscribeModule.subscribe', 'subscribe')];
?>

<h1>
    <small>
        <?php echo Yii::t('SubscribeModule.subscribe', 'subscribe'); ?>
    </small>
</h1>