<?php
/**
 *
 */
Yii::import('application.modules.subscribe.models.*');

class SubscribeWidget extends \yupe\widgets\YWidget
{
    public $title = 'Подписаться';
    public $view = 'subscribe-widget';
    public $formClassName = 'Subscribe';

    public function run()
    {
        $model = new $this->formClassName;
        if (isset($_POST[$this->formClassName])) {
            $model->attributes = $_POST[$this->formClassName];
            if($model->verify == ''){
                if ($model->save()) {
                    Yii::app()->user->setFlash('subscribe-success', 'Вы успешно подписались');
                    Yii::app()->controller->refresh();
                }
            }
        }
        $this->render($this->view, [
            'model' => $model,
            'title' => $this->title,
        ]);
    }
}
?>