<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('MailModule.mail', 'Заказы логотипов') => ['/mail/mailOrderLogoLayingBackend/index'],
    $model->name => ['/mail/mailOrderLogoLayingBackend/view', 'id' => $model->id],
    Yii::t('MailModule.mail', 'Редактирование'),
];

$this->pageTitle = Yii::t('MailModule.mail', 'Заказы логотипов - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MailModule.mail', 'Управление Заказами логотипов'), 'url' => ['/mail/mailOrderLogoLayingBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MailModule.mail', 'Добавить Заказ логотипа'), 'url' => ['/mail/mailOrderLogoLayingBackend/create']],
    ['label' => Yii::t('MailModule.mail', 'Заказ логотипа') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('MailModule.mail', 'Редактирование Заказа логотипа'), 'url' => [
        '/mail/mailOrderLogoLayingBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('MailModule.mail', 'Просмотреть Заказ логотипа'), 'url' => [
        '/mail/mailOrderLogoLayingBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('MailModule.mail', 'Удалить Заказ логотипа'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/mail/mailOrderLogoLayingBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('MailModule.mail', 'Вы уверены, что хотите удалить Заказ логотипа?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('MailModule.mail', 'Редактирование') . ' ' . Yii::t('MailModule.mail', 'Заказа логотипа'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>