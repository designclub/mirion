<?php
/**
 * Directions install migration
 * Класс миграций для модуля Directions:
 *
 **/
class m180421_143332_directions_add_column_title_breadcrumbs extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'title_breadcrumbs', 'string');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropColumn('{{page_page}}', 'title_breadcrumbs');
    }
}
