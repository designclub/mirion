<?php
/**
 * Directions install migration
 * Класс миграций для модуля Directions:
 *
 **/
class m180421_143331_directions_add_column_portfolio extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'name_desc', 'string');
        $this->addColumn('{{page_page}}', 'svg_icon', 'text');
        $this->addColumn('{{page_page}}', 'page_prev', 'integer COMMENT "Предыдущая страница"');
        $this->addColumn('{{page_page}}', 'page_next', 'integer COMMENT "Следующая страница"');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropColumn('{{page_page}}', 'name_desc');
        $this->dropColumn('{{page_page}}', 'svg_icon');
        $this->dropColumn('{{page_page}}', 'page_prev');
        $this->dropColumn('{{page_page}}', 'page_next');
    }
}
