<?php
/**
 * SiteMapWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');
Yii::import('application.modules.news.models.*');

/**
 * Class SiteMapWidget
 */
class SiteMapWidget extends yupe\widgets\YWidget
{
	public $notIds = "1";
    public $view = 'sitemap-widget';

    /**
     * @throws CException
     */
    public function run()
    {
    	$siteMapArray = [];
    	$array = [];
    	/*
    	 * Страницы
    	*/
    	$criteria = new CDbCriteria();

    	if($this->notIds){
	    	$notIds = explode(',', $this->notIds);
	        $criteria->addNotInCondition('id', $notIds);
    	}

    	$criteria->order = 't.order ASC';
    	$pages = Page::model()->published()->roots()->findAll($criteria);
    	foreach ($pages as $key => $page) {
    		$child = $this->pageList($page);

    		$siteMapArray[] = [
                'label' => $page->title,
                'url' =>  $page->getUrl(),
                'itemOptions' => ['class' => 'listItem '. (($child) ? 'submenuItem' : '')],
            ] + ($child ? [
                'items' => $child,
            ] : []);
		}

		/*
    	 * Новости
    	*/
    	$news = News::model()->published()->findAll();
    	$newsArray = [];
    	foreach ($news as $key => $news) {
    		$newsArray[] = [
                'label' => $news->title,
                'url' =>  $news->getUrl(),
            ];
		}
		$siteMapArray[] = [
            'label' => 'Новости',
            'url' =>  Yii::app()->createUrl('news/news/index'),
            'itemOptions' => ['class' => 'listItem submenuItem'],
        ];
        // ] + ['items' => $newsArray];

        $siteMapArray[] = [
            'label' => 'Отзывы',
            'url' =>  Yii::app()->createUrl('review/review/show'),
        ];

        $array[] = [
            'label' => 'Главная страница',
            'url' =>  '/',
        ] + ['items' => $siteMapArray];

    	$this->render($this->view,[
            'siteMapArray' => $array,
        ]);
        
    }

    private function pageList($page)
    {
    	$childPages = $page->childPages([
    		'condition' => 'childPages.status = :childStatus', 
    		'params' => [
    			':childStatus' => Page::STATUS_PUBLISHED
    		],
    		'order' => 'childPages.order ASC'
    	]);

 		if($childPages){
    		$resultArray = [];
 			foreach ($childPages as $item) {
 				$child = $this->pageList($item);

	    		$resultArray[] = [
	                'label' => $item->title,
	                'url' =>  $item->getUrl(),
	                'itemOptions' => ['class' => 'listItem '. (($child) ? 'submenuItem' : '')],
	            ] + ($child ? [
	                'items' => $child,
	            ] : []);
 			}
 			return $resultArray;
 		}

		return false;
    }
}
