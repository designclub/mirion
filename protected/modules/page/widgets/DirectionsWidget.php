<?php
/**
 * DirectionsWidget виджет для вывода направлений (портфолии)
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.page.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class DirectionsWidget
 */
class DirectionsWidget extends yupe\widgets\YWidget
{
    public $id;
    public $parent_id;

    public $limit = 12;
    /**
     * @var string
     */
    public $view = 'direction-widget';

    protected $models;
    protected $dataProvider;

    public function init()
    {

        
        if($this->id) {
            $this->models = Page::model()->published()->findByPk($this->id);
        } else{
            $criteria = new CDbCriteria();
            $criteria->order = 't.order ASC';

            if($this->limit){
                $criteria->limit = $this->limit;
            }

            $criteria->compare("parent_id", $this->parent_id);
            $this->models = Page::model()->published()->findAll($criteria);
        }

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view,[
            'models' => $this->models,
            'limit' => $this->limit,
        ]);
    }
}
