<?php
/**
 * News основная моделька для новостей
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.news.models
 * @since 0.1
 *
 */

/**
 * This is the model class for table "News".
 *
 * The followings are the available columns in table 'News':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $date
 * @property string $title
 * @property string $slug
 * @property string $short_text
 * @property string $full_text
 * @property integer $user_id
 * @property integer $status
 * @property integer $is_protected
 * @property string $link
 * @property string $image
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */

use yupe\components\Event;
use yupe\widgets\YPurifier;

/**
 * Class News
 */
class News extends yupe\models\YModel
{
    public $uploadCustomfield = 'customfield';
    public $uploadCustomfieldGallery = 'customfield/gallery';
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     *
     */
    const PROTECTED_NO = 0;
    /**
     *
     */
    const PROTECTED_YES = 1;

    public $title_short;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{news_news}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return News   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['title, slug, short_text, full_text, meta_title, meta_keywords, meta_description', 'filter', 'filter' => 'trim'],
            ['title, slug, meta_title, meta_keywords, meta_description', 'filter', 'filter' => [new YPurifier(), 'purify']],
            ['date, title, slug, full_text', 'required', 'on' => ['update', 'insert']],
            ['status, is_protected, category_id', 'numerical', 'integerOnly' => true],
            ['title, slug', 'length', 'max' => 150],
            ['lang', 'length', 'max' => 2],
            ['lang', 'default', 'value' => Yii::app()->sourceLanguage],
            ['lang', 'in', 'range' => array_keys(Yii::app()->getModule('yupe')->getLanguagesList())],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['slug', 'yupe\components\validators\YUniqueSlugValidator'],
            ['meta_title, meta_keywords, meta_description', 'length', 'max' => 250],
            ['link', 'length', 'max' => 250],
            ['link', 'yupe\components\validators\YUrlValidator'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('NewsModule.news', 'Bad characters in {attribute} field')
            ],
            ['category_id', 'default', 'setOnEmpty' => true, 'value' => null],
            [
                'id, meta_title, meta_keywords, meta_description, create_time, update_time, date, title, slug, short_text, full_text, user_id, status, is_protected, lang',
                'safe',
                'on' => 'search'
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('news');

        return [
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'category' => [self::BELONGS_TO, 'NewsCategory', 'category_id'],
            'user' => [self::BELONGS_TO, 'User', 'user_id'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => 't.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
            'protected' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_YES],
            ],
            'public' => [
                'condition' => 't.is_protected = :is_protected',
                'params' => [':is_protected' => self::PROTECTED_NO],
            ],
            'recent' => [
                'order' => 'create_time DESC',
                'limit' => 5,
            ]
        ];
    }

    /**
     * @param $num
     * @return $this
     */
    public function last($num)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'order' => 'date DESC',
                'limit' => $num,
            ]
        );

        return $this;
    }

    /**
     * @param $lang
     * @return $this
     */
    public function language($lang)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => 'lang = :lang',
                'params' => [':lang' => $lang],
            ]
        );

        return $this;
    }

    /**
     * @param $category_id
     * @return $this
     */
    public function category($category_id)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => 'category_id = :category_id',
                'params' => [':category_id' => $category_id],
            ]
        );

        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('NewsModule.news', 'Id'),
            'category_id' => Yii::t('NewsModule.news', 'Category'),
            'create_time' => Yii::t('NewsModule.news', 'Created at'),
            'update_time' => Yii::t('NewsModule.news', 'Updated at'),
            'date' => Yii::t('NewsModule.news', 'Date'),
            'title' => Yii::t('NewsModule.news', 'Title'),
            'slug' => Yii::t('NewsModule.news', 'Alias'),
            'image' => Yii::t('NewsModule.news', 'Image'),
            'link' => Yii::t('NewsModule.news', 'Link'),
            'lang' => Yii::t('NewsModule.news', 'Language'),
            'short_text' => Yii::t('NewsModule.news', 'Short text'),
            'full_text' => Yii::t('NewsModule.news', 'Full text'),
            'user_id' => Yii::t('NewsModule.news', 'Author'),
            'status' => Yii::t('NewsModule.news', 'Status'),
            'is_protected' => Yii::t('NewsModule.news', 'Access only for authorized'),
            'meta_title' => Yii::t('NewsModule.news', 'Page title (SEO)'),
            'meta_keywords' => Yii::t('NewsModule.news', 'Keywords (SEO)'),
            'meta_description' => Yii::t('NewsModule.news', 'Description (SEO)'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $this->slug = yupe\helpers\YText::translit($this->title);
        }

        if (!$this->lang) {
            $this->lang = Yii::app()->getLanguage();
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        $this->update_time = new CDbExpression('NOW()');
        $this->date = date('Y-m-d', strtotime($this->date));

        if ($this->getIsNewRecord()) {
            $this->create_time = $this->update_time;
            $this->user_id = Yii::app()->getUser()->getId();
        }

        /* Произвольные поля */
        $myfield = Yii::app()->getRequest()->getPost('MyCustomField');
        if(!empty($myfield)){
            ksort($myfield);

            $newmyfield = [];
            $count = 1;
            foreach ($myfield as $key => $value) {
                $newmyfield[$count] = $value;
                $newmyfield[$count]['image'] = $this->updateMyCustomFieldImage($count, $value['image']);
                $newmyfield[$count]['gallery'] = $this->updateMyCustomFieldGallery($count, $value['gallery']);
                $count++;
            }

            $this->data = serialize($newmyfield);
        } else if(is_array($this->data)) {
            $this->data = serialize($this->data);
        }
        /*==================*/

        return parent::beforeSave();
    }

    /**
     *
     */
    public function afterSave()
    {
        Yii::app()->eventManager->fire(NewsEvents::NEWS_AFTER_SAVE, new Event($this));

        return parent::afterSave();
    }

    /**
     *
     */
    public function afterDelete()
    {
        Yii::app()->eventManager->fire(NewsEvents::NEWS_AFTER_DELETE, new Event($this));

        parent::afterDelete();
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->date = date('d-m-Y', strtotime($this->date));

        /* Произвольные поля */
        if(!empty($this->data)){
            $this->data = unserialize($this->data);
        }
        /*==================*/
        
        return parent::afterFind();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        if ($this->date) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.slug', $this->slug, true);
        $criteria->compare('short_text', $this->short_text, true);
        $criteria->compare('full_text', $this->full_text, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('category_id', $this->category_id, true);
        $criteria->compare('is_protected', $this->is_protected);
        $criteria->compare('t.lang', $this->lang);
        $criteria->with = ['category'];

        return new CActiveDataProvider(get_class($this), [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => 'date DESC'],
        ]);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('NewsModule.news', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('NewsModule.news', 'Published'),
            self::STATUS_MODERATION => Yii::t('NewsModule.news', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('NewsModule.news', '*unknown*');
    }

    /**
     * @return array
     */
    public function getProtectedStatusList()
    {
        return [
            self::PROTECTED_NO => Yii::t('NewsModule.news', 'no'),
            self::PROTECTED_YES => Yii::t('NewsModule.news', 'yes'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getProtectedStatus()
    {
        $data = $this->getProtectedStatusList();

        return isset($data[$this->is_protected]) ? $data[$this->is_protected] : Yii::t('NewsModule.news', '*unknown*');
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    /**
     * @return string
     */
    public function getFlag()
    {
        return yupe\helpers\YText::langToflag($this->lang);
    }

    /**
     * @return bool
     */
    public function isProtected()
    {
        return $this->is_protected == self::PROTECTED_YES;
    }

    public function getCategoryList()
    {
        return CHtml::listData(NewsCategory::model()->published()->findAll(), 'id', 'name');
    }

    public function getShortTitle($length, $postfix='...', $encoding='UTF-8')
    {
        if (mb_strlen($this->title, $encoding) <= $length) {
            return $this->title;
        }

        $tmp = mb_substr($this->title, 0, $length, $encoding);
        return mb_substr($tmp, 0, mb_strripos($tmp, ' ', 0, $encoding), $encoding) . $postfix;
    }


    public function getUrl()
    {
        return Yii::app()->createUrl('news/news/view', ['slug' => $this->slug]);
    }

    public function prevPage()
    {
        $criteria = new CDbCriteria;
        $criteria->limit = 1;
        $criteria->compare('t.id', '<'.$this->id);
        $criteria->order = 'id DESC';

        $model = self::model()->find($criteria);
        
        if ($model===null) {
            $criteria = new CDbCriteria;
            $criteria->limit = 1;
            $criteria->order = 'id DESC';
            $model = self::model()->find($criteria);
        }
 
        if ($model) {
            return $model;
        }
    }

    public function nextPage()
    {
        $criteria = new CDbCriteria;
        $criteria->limit = 1;
        $criteria->compare('t.id', '>'.$this->id);
        $criteria->order = 'id ASC';
        
        $model = self::model()->find($criteria);

        if ($model===null) {
            $criteria = new CDbCriteria;
            $criteria->limit = 1;
            $criteria->order = 'id ASC';
            $model = self::model()->find($criteria);
        }
       
        if ($model) {
            return $model;
        }
    }

    /*******************************
    ////////////////////////////////
    ------ Произвольные поля -------
    ////////////////////////////////
    ****************************** */
    /*
     * Фунция загрузки изображения
    */
    public function updateMyCustomFieldImage($count, $name)
    {
        $delete = Yii::app()->getRequest()->getPost('myCustomField-delete-image-'.$count);
        $new_image = CUploadedFile::getInstancesByName('MyCustomField_'.$count);
        $path = Yii::getPathOfAlias("webroot.uploads.{$this->uploadCustomfield}").DIRECTORY_SEPARATOR;
        if (!empty($new_image) || !empty($delete)) {
            $del = unlink($path.$name);
            if($del == true && empty($new_image)){
                return '';
            }
            foreach($new_image as $key => $item) {
                $filename = substr(md5(microtime() . rand(0, 9999)), 0, 20) . '.' . $item->getExtensionName();
                $item->saveAs($path.$filename);
                return $filename;
            }
        }
        return $name;
    }
    /*
     * Фунция загрузки галереи для произвольного поля
    */
    public function updateMyCustomFieldGallery($count, $gallery)
    {
        $images = [];
        $new_images = CUploadedFile::getInstancesByName('MyCustomFieldGallery_'.$count);
        $path = Yii::getPathOfAlias("webroot.uploads.{$this->uploadCustomfieldGallery}").DIRECTORY_SEPARATOR;

        $newgallery = [];
        $new_pos = 1;
        foreach ($gallery as $key => $item) {
            $delete = Yii::app()->getRequest()->getPost('myCustomField-delete-galImage-'.$count.'_'.$key);
            if (!empty($delete)) {
                unlink($path.$item['image']);   
            } else {
                $newgallery[$key] = $item;
                $newgallery[$key]['position'] = $new_pos;
                $new_pos++;
            }
        }

        if (!empty($new_images)) {
            $pos = 1;
            if(count($newgallery) > 0){
                $pos = count($newgallery) + 1;  
            }
            foreach($new_images as $key => $item) {
                $filename = substr(md5(microtime() . rand(0, 9999)), 0, 20) . '.' . $item->getExtensionName();
                $item->saveAs($path.$filename);
                $images[$key]['image'] = $filename;
                $images[$key]['position'] = $pos;
                $pos++;
            }
        }

        if(!empty($newgallery)){
            $images = array_merge($newgallery, $images);
        }

        return $images;
    }

    /*
     * Функция получения значения произвольного поля
    */
    public function getAttributeValue($code)
    {
        $data = [];
        foreach ($this->data as $key => $value) {
            $data[$value['code']] = $value;
        }
        return (!empty($data[$code])) ? $data[$code] : false;
    }

    /*
     * Фунция получения url изображения
    */
    public function getFieldImageUrl($width = 0, $height = 0, $crop = true, $name)
    {
        $file = Yii::getPathOfAlias('webroot')."/uploads/{$this->uploadCustomfield}/{$name}";
        if(file_exists($file)){
            if ($width || $height) {
                return Yii::app()->thumbnailer->thumbnail(
                    $file,
                    $this->uploadCustomfield,
                    $width,
                    $height,
                    $crop
                );
            }

            return "/uploads/{$this->uploadCustomfield}/{$name}";
        }
        return false;
    }
    /*
     * Фунция получения url изображения
    */
    public function getFieldGalImageUrl($width = 0, $height = 0, $crop = true, $name)
    {
        $file = Yii::getPathOfAlias('webroot')."/uploads/{$this->uploadCustomfieldGallery}/{$name}";
        if(file_exists($file)){
            if ($width || $height) {
                return Yii::app()->thumbnailer->thumbnail(
                    $file,
                    $this->uploadCustomfieldGallery,
                    $width,
                    $height,
                    $crop
                );
            }

            return "/uploads/{$this->uploadCustomfieldGallery}/{$name}";
        }
        return false;
    }
    
    /* Преобразование изображения в webp и вовзрат пути до него */
    public function geFieldImageWebp($width = 0, $height = 0, $crop = true, $name)
    {
        // Получаем изображение
        $file = $this->getFieldImageUrl($width, $height, $crop, $name);
        // Получаем массив, где есть путь до папки, имя файла и расширение
        $pathinfo = pathinfo($file);
        // Получаем относительный путь к изображению
        $relativefile = str_replace(Yii::app()->request->getHostInfo(), '', $file);
        // Получаем абсолютный путь до изображения
        $fullpathfile = Yii::getPathOfAlias('webroot').$relativefile;
        // Задаем путь к изображению webp
        $webppath = dirname($fullpathfile).'/'.$pathinfo['filename'].'.webp';

        // В зависимости от расширения, преобразуем изображение в webp
        switch ($pathinfo['extension']) {
            case 'jpeg':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'jpg':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'png':
                $img = imagecreatefrompng($fullpathfile);
                break;
            case 'gif':
                $img = imagecreatefromgif($fullpathfile);
                break;
            case 'JPEG':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'JPG':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'PNG':
                $img = imagecreatefrompng($fullpathfile);
                break;
            case 'gif':
                $img = imagecreatefromgif($fullpathfile);
                break;
        }

        // Проверяем наличие файла, и если его нет - преобразуем в webp
        if(!file_exists($webppath)){
            imagepalettetotruecolor($img);
            imagewebp($img, $webppath, 100);
            //imagedestroy($img);
        }

        // Возвращаем путь к webp изображению
        return dirname($file).'/'.basename($webppath);
    }
    public function geFieldGalImageWebp($width = 0, $height = 0, $crop = true, $name)
    {
        // Получаем изображение
        $file = $this->getFieldGalImageUrl($width, $height, $crop, $name);
        // Получаем массив, где есть путь до папки, имя файла и расширение
        $pathinfo = pathinfo($file);
        // Получаем относительный путь к изображению
        $relativefile = str_replace(Yii::app()->request->getHostInfo(), '', $file);
        // Получаем абсолютный путь до изображения
        $fullpathfile = Yii::getPathOfAlias('webroot').$relativefile;
        // Задаем путь к изображению webp
        $webppath = dirname($fullpathfile).'/'.$pathinfo['filename'].'.webp';

        // В зависимости от расширения, преобразуем изображение в webp
        switch ($pathinfo['extension']) {
            case 'jpeg':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'jpg':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'png':
                $img = imagecreatefrompng($fullpathfile);
                break;
            case 'gif':
                $img = imagecreatefromgif($fullpathfile);
                break;
            case 'JPEG':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'JPG':
                $img = imagecreatefromjpeg($fullpathfile);
                break;
            case 'PNG':
                $img = imagecreatefrompng($fullpathfile);
                break;
            case 'gif':
                $img = imagecreatefromgif($fullpathfile);
                break;
        }

        // Проверяем наличие файла, и если его нет - преобразуем в webp
        if(!file_exists($webppath)){
            imagepalettetotruecolor($img);
            imagewebp($img, $webppath, 100);
            //imagedestroy($img);
        }

        // Возвращаем путь к webp изображению
        return dirname($file).'/'.basename($webppath);
    }
    /*********************************
    *************** END **************
    *********************************/
}
