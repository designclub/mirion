<?php

/**
 * Виджет вывода последних новостей
 *
 * @category YupeWidget
 * @package  yupe.modules.news.widgets
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     https://yupe.ru
 *
 **/
Yii::import('application.modules.news.models.*');

/**
 * Class ReadAlsoNewsWidget
 */
class ReadAlsoNewsWidget extends yupe\widgets\YWidget
{
    public $id;
    public $limit = 2;

    /**
     * @var string
     */
    public $view = 'readalso-widget';

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria;
        $criteria->limit = $this->limit;
        $criteria->compare('t.id', '>'.$this->id);
        $criteria->order = 'id ASC';

        $models = News::model()->published()->findAll($criteria);

        if (!$models) {
            $criteria = new CDbCriteria;
            $criteria->limit = $this->limit;
            $criteria->compare('t.id', '<'.$this->id);
            $criteria->order = 'id DESC';

            $models = News::model()->published()->findAll($criteria);
        }

        $this->render($this->view, ['models' => $models]);
    }
}
