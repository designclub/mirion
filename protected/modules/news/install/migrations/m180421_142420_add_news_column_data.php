<?php
/**
 * Directions install migration
 * Класс миграций для модуля Directions:
 *
 **/
class m180421_142420_add_news_column_data extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->addColumn('{{news_news}}', 'data', 'longtext');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropColumn('{{news_news}}', 'data');
    }
}
