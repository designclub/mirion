<?php
return [
	'key'        => '6LcqZfwUAAAAAOJHqjfhEsJIDF2uSAF-jHy9pi_U',
    'secretkey'  => '6LcqZfwUAAAAAOVH6iZFGHq6ZxeZiRue6g9UBEAQ',
    'runtimeWidgets' => [
        'application.modules.contentblock.widgets.ContentBlock',
        'application.modules.contentblock.widgets.ContentMyBlock',
        'application.modules.gallery.widgets.Gallery',
        'application.modules.gallery.widgets.NewGallery',
        'application.modules.gallery.widgets.CustomField',
        'application.modules.page.widgets.Directions',
        'application.modules.page.widgets.SiteMap',
    ],
];
