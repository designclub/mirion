<?php
/**
 * Файл настроек для модуля subscribe
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.subscribe.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.subscribe.SubscribeModule',
    ],
    'import'    => [
    	'application.modules.subscribe.SubscribeModule',
    ],
    'component' => [],
    'rules'     => [
        '/subscribe' => 'subscribe/subscribe/index',
    ],
];