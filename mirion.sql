/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50647
Source Host           : localhost:3306
Source Database       : mirion

Target Server Type    : MYSQL
Target Server Version : 50647
File Encoding         : 65001

Date: 2021-03-05 11:56:38
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `yupe_blog_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_blog`;
CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  KEY `ix_yupe_blog_blog_status` (`status`),
  KEY `ix_yupe_blog_blog_type` (`type`),
  KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  KEY `ix_yupe_blog_blog_lang` (`lang`),
  KEY `ix_yupe_blog_blog_slug` (`slug`),
  KEY `ix_yupe_blog_blog_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post`;
CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  KEY `ix_yupe_blog_post_status` (`status`),
  KEY `ix_yupe_blog_post_access_type` (`access_type`),
  KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  KEY `ix_yupe_blog_post_lang` (`lang`),
  KEY `ix_yupe_blog_post_slug` (`slug`),
  KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  KEY `ix_yupe_blog_post_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post_to_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post_to_tag`;
CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`),
  CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post_to_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_tag`;
CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_user_to_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_user_to_blog`;
CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_user_to_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_category_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_category_category`;
CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`),
  CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_category_category
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_comment_comment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_comment_comment`;
CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_comment_comment_status` (`status`),
  KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_yupe_comment_comment_model` (`model`),
  KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  KEY `ix_yupe_comment_comment_level` (`level`),
  KEY `ix_yupe_comment_comment_root` (`root`),
  KEY `ix_yupe_comment_comment_lft` (`lft`),
  KEY `ix_yupe_comment_comment_rgt` (`rgt`),
  CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_comment_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_contentblock_content_block`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_contentblock_content_block`;
CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_contentblock_content_block
-- ----------------------------
INSERT INTO `yupe_contentblock_content_block` VALUES ('1', 'Телефон:', 'telefon', '1', '<a href=\"tel:+78127483969\">\r\n  <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n  <path d=\"M19.5836 23.9543C20.2396 24.0126 20.9 23.8244 21.427 23.4229C22.0162 22.9742 22.3917 22.2935 22.4571 21.5559L22.7707 18.0213C22.8897 16.6799 21.9884 15.4667 20.6743 15.1994C19.9009 15.042 19.1393 14.8123 18.4103 14.5167C17.4979 14.1466 16.4656 14.3059 15.7166 14.9327L14.1574 16.2377C11.5477 14.4937 9.46346 12.0036 8.2063 9.12824L9.76572 7.82312C10.5145 7.19622 10.8531 6.2081 10.6494 5.24465C10.4867 4.47498 10.3947 3.68482 10.3759 2.89593C10.3443 1.55511 9.30872 0.4543 7.96733 0.335285L4.43291 0.0216898C3.69519 -0.0437649 2.95901 0.205947 2.41355 0.706956C1.86571 1.20982 1.55307 1.92561 1.55551 2.67045C1.56385 5.22728 2.01878 7.73563 2.90771 10.126C3.7718 12.4499 5.01967 14.5956 6.61687 16.5038C8.21394 18.4119 10.1065 20.0183 12.242 21.2779C14.4387 22.5737 16.8277 23.4633 19.3431 23.9218C19.423 23.9365 19.5031 23.9472 19.5836 23.9543ZM17.486 16.0816C17.5746 16.0895 17.6636 16.1108 17.7494 16.1456C18.5785 16.4819 19.4446 16.7431 20.3239 16.922C20.7604 17.0108 21.0596 17.4166 21.0198 17.866L20.7062 21.4005C20.684 21.6509 20.5616 21.8724 20.3617 22.0246C20.1595 22.1788 19.9097 22.2384 19.6584 22.1924C10.2191 20.4721 3.34493 12.2592 3.3133 2.66456C3.31239 2.40915 3.41517 2.17369 3.60236 2.00169C3.78755 1.83186 4.02723 1.75041 4.27756 1.77262L7.81198 2.08622C8.26134 2.12609 8.60815 2.49202 8.61881 2.93745C8.64 3.83451 8.74461 4.73305 8.92959 5.60838C8.99855 5.93436 8.88677 6.26649 8.63747 6.47516L6.09868 8.60015L6.32909 9.18875C7.76575 12.8582 10.4076 16.014 13.7674 18.0748L14.3062 18.4055L16.8449 16.2805C17.0246 16.13 17.2552 16.0612 17.486 16.0816Z\" fill=\"#161616\"/>\r\n  </svg>\r\n  <span>+7 (812) 748-39-69</span>\r\n</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('2', 'E-mail:', 'e-mail', '1', '<a href=\"mailto:info@mirrion.ru\">\r\n  <svg width=\"17\" height=\"17\" viewBox=\"0 0 17 17\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n    <path d=\"M16.3928 2.42871H0.607154C0.271827 2.42871 0 2.70054 0 3.03583V13.9644C0 14.2997 0.271827 14.5715 0.607154 14.5715H16.3928C16.7282 14.5715 17 14.2997 17 13.9644V3.03583C17 2.70054 16.7282 2.42871 16.3928 2.42871ZM15.3971 3.64298L8.49998 8.94821L1.60286 3.64298H15.3971ZM15.7857 13.3573H1.21427V4.87608L8.13025 10.1959C8.34833 10.3633 8.65167 10.3633 8.86975 10.1959L15.7857 4.87608V13.3573Z\" fill=\"161616\"/></svg>\r\n  <span>info@mirrion.ru</span>\r\n</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('3', 'Адрес:', 'adres', '1', 'г. Санкт-Петербург, <br class=\"br\">ул. Кондратенко, 3', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('4', 'Политика конфиденциальности', 'politika-konfidencialnosti', '1', '#', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('5', 'Скрипты в шапке', 'skripty-v-shapke', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('6', 'Скрипты в футере', 'skripty-v-futere', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('7', 'График работы:', 'grafik-raboty', '1', 'Пн-Пт, 10:00 до 18:00', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('8', 'Соц сети', 'soc-seti', '1', '<div class=\"soc-box\">\r\n  <span>Мы в соц. сетях</span>\r\n  <a href=\"#\">[[w:ContentMyBlock|id=9]]</a>\r\n  <a href=\"#\">[[w:ContentMyBlock|id=10]]</a>\r\n</div>', '', null, '0');
INSERT INTO `yupe_contentblock_content_block` VALUES ('9', 'Карта яндекс', 'karta-yandeks', '1', 'https://yandex.ru/map-widget/v1/?um=constructor%3Ad9f1b9f7bed986838ec6a52e636dda9bae91f348cc0863138e9eff866c876c04&amp;source=constructor', '', null, '1');

-- ----------------------------
-- Table structure for `yupe_gallery_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_gallery`;
CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  KEY `ix_yupe_gallery_gallery_sort` (`sort`),
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_gallery
-- ----------------------------
INSERT INTO `yupe_gallery_gallery` VALUES ('1', 'Наши партнеры', '<p>Наши партнеры</p>', '2', '1', null, null, '1');
INSERT INTO `yupe_gallery_gallery` VALUES ('2', 'Мы в соц. сетях', '<p>Мы в соц. сетях</p>', '2', '1', null, null, '2');

-- ----------------------------
-- Table structure for `yupe_gallery_image_to_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_image_to_gallery`;
CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_image_to_gallery
-- ----------------------------
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('1', '1', '1', '2020-10-16 12:14:47', '1');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('2', '2', '1', '2020-10-16 12:14:50', '2');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('3', '3', '1', '2020-10-16 12:14:52', '3');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('4', '4', '1', '2020-10-16 12:14:54', '4');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('5', '5', '1', '2020-10-16 12:14:57', '5');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('6', '6', '1', '2020-10-16 12:14:59', '6');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('7', '7', '2', '2020-10-19 10:15:27', '8');
INSERT INTO `yupe_gallery_image_to_gallery` VALUES ('8', '8', '2', '2020-10-19 10:16:43', '7');

-- ----------------------------
-- Table structure for `yupe_image_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_image_image`;
CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`),
  CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_image_image
-- ----------------------------
INSERT INTO `yupe_image_image` VALUES ('1', null, null, 'partner1', '', 'c0fcc4b62f823c8866f7692546f5d413.jpg', '2020-10-16 12:14:47', '1', 'partner1', '0', '1', '1');
INSERT INTO `yupe_image_image` VALUES ('2', null, null, 'partner1', '', '7ab86a5156cd0a28560a7ed782d0939e.jpg', '2020-10-16 12:14:50', '1', 'partner1', '0', '1', '2');
INSERT INTO `yupe_image_image` VALUES ('3', null, null, 'partner2', '', '4d32c20fd91e90aa083107148e745d52.jpg', '2020-10-16 12:14:52', '1', 'partner2', '0', '1', '3');
INSERT INTO `yupe_image_image` VALUES ('4', null, null, 'partner2', '', '212c738a5ff781da2bc7a9530ae4846e.jpg', '2020-10-16 12:14:54', '1', 'partner2', '0', '1', '4');
INSERT INTO `yupe_image_image` VALUES ('5', null, null, 'partner2', '', '899038857d670ca730f3e4381d0c2774.jpg', '2020-10-16 12:14:57', '1', 'partner2', '0', '1', '5');
INSERT INTO `yupe_image_image` VALUES ('6', null, null, 'partner1', '', '1c2e26bc7c3b28510ac14fe4fc520b9b.jpg', '2020-10-16 12:14:59', '1', 'partner1', '0', '1', '6');
INSERT INTO `yupe_image_image` VALUES ('7', null, null, 'icon-instagram', 'https://www.instagram.com/mirrion.ru/', '3c8c63fae0923b5f7356ce604d2a0fd1.svg', '2020-10-19 10:15:27', '1', 'icon-instagram', '0', '1', '7');
INSERT INTO `yupe_image_image` VALUES ('8', null, null, 'icon-facebook', '', '81b7fa78cbeff72c30a73d2b6136e1af.svg', '2020-10-19 10:16:43', '1', 'icon-facebook', '0', '1', '8');

-- ----------------------------
-- Table structure for `yupe_mail_mail_event`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_event`;
CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_event
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_mail_mail_template`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_template`;
CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`),
  CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_template
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_menu_menu`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu`;
CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu
-- ----------------------------
INSERT INTO `yupe_menu_menu` VALUES ('1', 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', '1');
INSERT INTO `yupe_menu_menu` VALUES ('2', 'Нижнее меню', 'nizhnee-menyu', 'Нижнее меню', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu_item`;
CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`),
  CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu_item
-- ----------------------------
INSERT INTO `yupe_menu_menu_item` VALUES ('12', '0', '1', '1', 'Меню', '/menyu', null, null, null, null, null, null, '0', '0', '1', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('13', '0', '1', '1', 'О нас', '/company', null, null, null, null, null, null, '0', '0', '2', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('14', '0', '1', '1', 'Портфолио', '/portfolio', null, null, null, null, null, null, '0', '0', '3', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('15', '0', '1', '1', 'Магазин', '/magazin', null, null, null, null, null, null, '0', '0', '4', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('16', '0', '1', '1', 'Контакты', '/kontakty', null, null, null, null, null, null, '0', '0', '5', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('17', '0', '1', '1', 'Венецианские', '/venecianskie', null, null, null, null, null, null, '0', '0', '6', '0', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('18', '0', '2', '1', 'О компании', '/company', '', '', '', '', '', '', '', '0', '7', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('19', '0', '2', '1', 'Портфолио', '/portfolio', '', '', '', '', '', '', '', '0', '8', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('20', '0', '2', '1', 'Новости', '/news', '', '', '', '', '', '', '', '0', '9', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('22', '0', '2', '1', 'Отзывы', '/review', '', '', '', '', '', '', '', '0', '11', '1', '', '', null);

-- ----------------------------
-- Table structure for `yupe_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_migrations`;
CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_migrations
-- ----------------------------
INSERT INTO `yupe_migrations` VALUES ('1', 'user', 'm000000_000000_user_base', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('2', 'user', 'm131019_212911_user_tokens', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('3', 'user', 'm131025_152911_clean_user_table', '1545824979');
INSERT INTO `yupe_migrations` VALUES ('4', 'user', 'm131026_002234_prepare_hash_user_password', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('5', 'user', 'm131106_111552_user_restore_fields', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('6', 'user', 'm131121_190850_modify_tokes_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('7', 'user', 'm140812_100348_add_expire_to_token_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('8', 'user', 'm150416_113652_rename_fields', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('9', 'user', 'm151006_000000_user_add_phone', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('10', 'yupe', 'm000000_000000_yupe_base', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('11', 'yupe', 'm130527_154455_yupe_change_unique_index', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('12', 'yupe', 'm150416_125517_rename_fields', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('13', 'yupe', 'm160204_195213_change_settings_type', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('14', 'category', 'm000000_000000_category_base', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('15', 'category', 'm150415_150436_rename_fields', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('16', 'image', 'm000000_000000_image_base', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('17', 'image', 'm150226_121100_image_order', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('18', 'image', 'm150416_080008_rename_fields', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('19', 'page', 'm000000_000000_page_base', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('20', 'page', 'm130115_155600_columns_rename', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('21', 'page', 'm140115_083618_add_layout', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('22', 'page', 'm140620_072543_add_view', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('23', 'page', 'm150312_151049_change_body_type', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('24', 'page', 'm150416_101038_rename_fields', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('25', 'page', 'm180224_105407_meta_title_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('26', 'page', 'm180421_143324_update_page_meta_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('27', 'mail', 'm000000_000000_mail_base', '1545824991');
INSERT INTO `yupe_migrations` VALUES ('28', 'comment', 'm000000_000000_comment_base', '1545824992');
INSERT INTO `yupe_migrations` VALUES ('29', 'comment', 'm130704_095200_comment_nestedsets', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('30', 'comment', 'm150415_151804_rename_fields', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('31', 'notify', 'm141031_091039_add_notify_table', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('32', 'blog', 'm000000_000000_blog_base', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('33', 'blog', 'm130503_091124_BlogPostImage', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('34', 'blog', 'm130529_151602_add_post_category', '1545825002');
INSERT INTO `yupe_migrations` VALUES ('35', 'blog', 'm140226_052326_add_community_fields', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('36', 'blog', 'm140714_110238_blog_post_quote_type', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('37', 'blog', 'm150406_094809_blog_post_quote_type', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('38', 'blog', 'm150414_180119_rename_date_fields', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('39', 'blog', 'm160518_175903_alter_blog_foreign_keys', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('40', 'blog', 'm180421_143937_update_blog_meta_column', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('41', 'blog', 'm180421_143938_add_post_meta_title_column', '1545825006');
INSERT INTO `yupe_migrations` VALUES ('42', 'contentblock', 'm000000_000000_contentblock_base', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('43', 'contentblock', 'm140715_130737_add_category_id', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('44', 'contentblock', 'm150127_130425_add_status_column', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('45', 'menu', 'm000000_000000_menu_base', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('46', 'menu', 'm121220_001126_menu_test_data', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('47', 'menu', 'm160914_134555_fix_menu_item_default_values', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('48', 'menu', 'm181214_110527_menu_item_add_entity_fields', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('49', 'gallery', 'm000000_000000_gallery_base', '1545825012');
INSERT INTO `yupe_migrations` VALUES ('50', 'gallery', 'm130427_120500_gallery_creation_user', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('51', 'gallery', 'm150416_074146_rename_fields', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('52', 'gallery', 'm160514_131314_add_preview_to_gallery', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('53', 'gallery', 'm160515_123559_add_category_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('54', 'gallery', 'm160515_151348_add_position_to_gallery_image', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('55', 'gallery', 'm181224_072816_add_sort_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('60', 'page', 'm180421_143325_add_column_image', '1547437100');
INSERT INTO `yupe_migrations` VALUES ('61', 'rbac', 'm140115_131455_auth_item', '1547444421');
INSERT INTO `yupe_migrations` VALUES ('62', 'rbac', 'm140115_132045_auth_item_child', '1547444422');
INSERT INTO `yupe_migrations` VALUES ('63', 'rbac', 'm140115_132319_auth_item_assign', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('64', 'rbac', 'm140702_230000_initial_role_data', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('65', 'page', 'm180421_143326_add_column_body_Short', '1574077121');
INSERT INTO `yupe_migrations` VALUES ('66', 'page', 'm180421_143328_add_page_image_tbl', '1574077121');
INSERT INTO `yupe_migrations` VALUES ('67', 'page', 'm180421_143329_add_page_title_page_h1', '1574077122');
INSERT INTO `yupe_migrations` VALUES ('68', 'page', 'm180421_143330_directions_add_column_data', '1590064486');
INSERT INTO `yupe_migrations` VALUES ('70', 'slider', 'm000000_000000_slider_base', '1602567309');
INSERT INTO `yupe_migrations` VALUES ('71', 'slider', 'm000001_000001_slider_add_column', '1602567309');
INSERT INTO `yupe_migrations` VALUES ('72', 'slider', 'm000001_000002_slider_add_table_category', '1602567310');
INSERT INTO `yupe_migrations` VALUES ('73', 'slider', 'm000001_000003_slider_add_column_image_big', '1602568360');
INSERT INTO `yupe_migrations` VALUES ('74', 'page', 'm180421_143331_directions_add_column_portfolio', '1602757325');
INSERT INTO `yupe_migrations` VALUES ('77', 'news', 'm000000_000000_news_base', '1602827497');
INSERT INTO `yupe_migrations` VALUES ('78', 'news', 'm150416_081251_rename_fields', '1602827497');
INSERT INTO `yupe_migrations` VALUES ('79', 'news', 'm180224_105353_meta_title_column', '1602827498');
INSERT INTO `yupe_migrations` VALUES ('80', 'news', 'm180421_142416_update_news_meta_column', '1602827498');
INSERT INTO `yupe_migrations` VALUES ('81', 'news', 'm180421_142417_add_table_category', '1602827499');
INSERT INTO `yupe_migrations` VALUES ('82', 'news', 'm180421_142418_add_foreigkey', '1602827499');
INSERT INTO `yupe_migrations` VALUES ('83', 'news', 'm180421_142419_update_seo_meta_column', '1602827500');
INSERT INTO `yupe_migrations` VALUES ('84', 'news', 'm180421_142420_add_news_column_data', '1602827500');
INSERT INTO `yupe_migrations` VALUES ('85', 'review', 'm000000_000000_review_base', '1602848003');
INSERT INTO `yupe_migrations` VALUES ('86', 'review', 'm000000_000001_review_add_column', '1602848004');
INSERT INTO `yupe_migrations` VALUES ('87', 'review', 'm000000_000002_review_add_column_pos', '1602848004');
INSERT INTO `yupe_migrations` VALUES ('88', 'review', 'm000000_000003_review_add_column_name_service', '1602848005');
INSERT INTO `yupe_migrations` VALUES ('89', 'review', 'm000000_000004_review_add_column_rating', '1602848005');
INSERT INTO `yupe_migrations` VALUES ('90', 'review', 'm000000_000005_review_rename_column', '1602848006');
INSERT INTO `yupe_migrations` VALUES ('91', 'review', 'm000000_000006_add_table_images', '1602848007');
INSERT INTO `yupe_migrations` VALUES ('92', 'review', 'm000000_000007_rename_table_images_name', '1602848007');
INSERT INTO `yupe_migrations` VALUES ('93', 'review', 'm000000_000008_add_column_count', '1602848008');
INSERT INTO `yupe_migrations` VALUES ('94', 'review', 'm000000_000009_add_column_name_desc', '1602848008');
INSERT INTO `yupe_migrations` VALUES ('95', 'page', 'm180421_143332_directions_add_column_title_breadcrumbs', '1603258572');
INSERT INTO `yupe_migrations` VALUES ('96', 'subscribe', 'm000000_000000_subscribe_base', '1603861851');
INSERT INTO `yupe_migrations` VALUES ('97', 'sitemap', 'm141004_130000_sitemap_page', '1604043506');
INSERT INTO `yupe_migrations` VALUES ('98', 'sitemap', 'm141004_140000_sitemap_page_data', '1604043507');

-- ----------------------------
-- Table structure for `yupe_news_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_news_category`;
CREATE TABLE `yupe_news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `slug` varchar(255) DEFAULT NULL COMMENT 'Slug',
  `description` text COMMENT 'Описание',
  `icon` varchar(255) DEFAULT NULL COMMENT 'Иконка',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `meta_title` varchar(250) DEFAULT NULL COMMENT 'Title (SEO)',
  `meta_keywords` text COMMENT 'Ключевые слова (SEO)',
  `meta_description` text COMMENT 'Описание (SEO)',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_news_category
-- ----------------------------
INSERT INTO `yupe_news_category` VALUES ('1', '1', '1', '2020-10-16 10:53:37', '2020-10-19 14:51:31', 'Новости', 'Новости', 'novosti', '', null, null, '', '', '', '1', '1');

-- ----------------------------
-- Table structure for `yupe_news_news`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_news_news`;
CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`),
  CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_news_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_news_news
-- ----------------------------
INSERT INTO `yupe_news_news` VALUES ('1', '1', 'ru', '2020-10-16 10:55:13', '2020-10-16 10:57:01', '2020-10-01', 'Выполнен проект для  #tsum торговое оборудование в глянцевом металлике', 'vypolnen-proekt-dlya-tsum-torgovoe-oborudovanie-v-glyancevom-metallike', '', '<p>Раздел в стадии наполнения!</p>', '533efd42ec3a3f3ff1489ec2815b4eb4.jpg', '', '1', '1', '0', '', '', '', null);
INSERT INTO `yupe_news_news` VALUES ('2', '1', 'ru', '2020-10-16 10:55:42', '2020-10-16 10:55:42', '2020-10-03', 'Выполнен проект для  #tsum торговое оборудование в глянцевом металлике', 'vypolnen-proekt-dlya-tsum-torgovoe-oborudovanie-v-glyancevom-metallike-2', '', '<p>Раздел в стадии наполнения!</p>', '564276f34dd754d4918a64a97ac84d96.jpg', '', '1', '1', '0', '', '', '', null);
INSERT INTO `yupe_news_news` VALUES ('3', '1', 'ru', '2020-10-16 10:56:20', '2020-10-16 10:56:20', '2020-10-05', 'Выполнен проект для  #tsum торговое оборудование в глянцевом металлике', 'vypolnen-proekt-dlya-tsum-torgovoe-oborudovanie-v-glyancevom-metallike-3', '', '<p>Раздел в стадии наполнения!</p>', '974a8633e92e567488703cf8eee1e79f.jpg', '', '1', '1', '0', '', '', '', null);
INSERT INTO `yupe_news_news` VALUES ('4', '1', 'ru', '2020-10-16 10:56:48', '2020-10-19 15:31:51', '2020-10-10', 'Выполнен проект для  #tsum торговое оборудование в глянцевом металлике', 'vypolnen-proekt-dlya-tsum-torgovoe-oborudovanie-v-glyancevom-metallike-4', '', '<p>Офис компании JetBrains, номинированный на премию Best Office Awards 2019. Главная фишка рабочих зон офиса &ndash; эффект soft touch, выраженный в скруглении внутренних и внешних углов при стыковке перегородок. Интересным решением стали ритмические и градиентные структуры отделочных поверхностей.</p>\r\n<div>Атриумы решены достаточно аскетично &ndash; главными акцентами стали природные материалы и живая зелень. В центре первого этажа размещены альпийские горки, фитокомпозиции и живые карликовые деревья. Стержнями всего офиса являются атриумы, пронизывающие обе башни.</div>\r\n<div>[[w:CustomField|module=news;id=4;code=foto1]]</div>\r\n<p>Офисы компании представлены в пяти странах по всему миру, а петербургский офис JetBrains &mdash; самое большое рабочее пространство компании. Собственная пекарня, закрытый кинотеатр, фитнес зона, два этажа для отдыха: акустическая музыкальная комната, зимние сады, бильярд, возможность выпить чашечку кофе на террасе с видом на Финский залив&hellip; чего ещё желать от лучшего офиса Санкт-Петербурга?</p>\r\n<div>Новый офис JetBrains. Это рабочее пространство, разместившееся на территории бывшего апарт-отеля общей площадью более 20 тысяч квадратных метров. Кабинеты и общественные пространства самого большого офиса компании распределены на двух 8-этажных башнях и стилобате Space. Одной из особенностей проекта стало его расположение: здание находится на берегу Финского залива, поэтому из окон открываются панорамные виды на море и Неву, Зенит-Арену, эстакаду ЗСД и небоскреб Лахта. По этой же причине верхний этаж обеих башен было решено задействовать под зоны отдыха с выходом на круговую открытую террасу.</div>\r\n<div>[[w:CustomField|module=news;id=4;code=foto2]]</div>\r\n<div>Кроме террас с панорамным видом на верхних этажах расположена большая рекреационная зона, где есть бильярдный зал, игровая и музыкальная комнаты, библиотеки, комнаты отдыха, кикерные, кофепоинты и даже свой небольшой кинозал. На нижележащих этажах башен размещены рабочие кабинеты сотрудников, переговорные комнаты, рассчитанные на различное количество участников, телефонные комнаты, принтерные, а также зоны неформального общения и разноформатные кофепоинты.</div>\r\n<div>[[w:CustomField|module=news;id=4;code=foto3]]</div>\r\n<p>На первом этаже расположились спортивный зал с массажными кабинетами, зона для игры в настольный теннис и ещё одна открытая терраса с панорамным видом, куда можно попасть из двухуровневой столовой со своей собственной кухней и пекарней. Весь первый этаж и соединяющую башни пристройку также заняли общественные пространства: просторная входная группа, рецепция и интерактивный экран, ориентированный на главный фасад здания, учебные комнаты и три конференц-зала, самый большой из которых рассчитан на 400 мест.</p>\r\n<blockquote>\r\n<p>От первого впечатления зависит оценка любого заведения, поэтому владельцам бизнеса важно заботиться не только о высоком качестве услуг, но и о дизайне помещения. Какой бы эффективной ни была задумка, важно подобрать мебель, которая будет удобной, качественной и гармоничной в выбранном варианте интерьера. Единственная в своем роде. Эксклюзивная, создающая комфорт и впечатление...</p>\r\n</blockquote>', 'c3a19785529d6e31390c0277edbef5ed.jpg', '', '1', '1', '0', '', '', '', 'a:3:{i:1;a:6:{s:4:\"name\";s:9:\"Фото1\";s:4:\"code\";s:5:\"foto1\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:4:{s:5:\"image\";s:24:\"8af5107d4a78a5cd3a18.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";}}s:5:\"image\";N;}i:2;a:6:{s:4:\"name\";s:9:\"Фото2\";s:4:\"code\";s:5:\"foto2\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:4:{s:5:\"image\";s:24:\"cd206238f6d87e6920b3.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";}}s:5:\"image\";N;}i:3;a:6:{s:4:\"name\";s:9:\"Фото3\";s:4:\"code\";s:5:\"foto3\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:4:{s:5:\"image\";s:24:\"e38b1450a1a65b320ebf.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";}}s:5:\"image\";N;}}');

-- ----------------------------
-- Table structure for `yupe_notify_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_notify_settings`;
CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_notify_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_page_page`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page`;
CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text,
  `name_h1` varchar(255) DEFAULT NULL,
  `data` longtext,
  `name_desc` varchar(255) DEFAULT NULL,
  `svg_icon` text,
  `page_prev` int(11) DEFAULT NULL COMMENT 'Предыдущая страница',
  `page_next` int(11) DEFAULT NULL COMMENT 'Следующая страница',
  `title_breadcrumbs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page
-- ----------------------------
INSERT INTO `yupe_page_page` VALUES ('1', null, 'ru', null, '2019-01-14 09:39:32', '2020-10-30 16:04:50', '1', '1', 'Главная', 'Главная', 'glavnaya', '<p>TEST</p>', '', '', '1', '0', '1', '', '', 'MIRRION', null, null, '', '', 'a:7:{i:1;a:8:{s:4:\"name\";s:31:\"Наши направления\";s:4:\"code\";s:10:\"directions\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:5:\"image\";s:24:\"ad08487394e8204aa04b.jpg\";s:7:\"gallery\";a:0:{}}i:2;a:8:{s:4:\"name\";s:36:\"Mirrion зеркала, стекло\";s:4:\"code\";s:14:\"mirror-gallery\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"butName\";s:14:\"Магазин\";s:7:\"butLink\";s:1:\"#\";s:7:\"gallery\";a:5:{i:0;a:5:{s:5:\"image\";s:24:\"1920ebb10e92dbe95bf8.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"b8cfec9f60db50b00075.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:2;a:5:{s:5:\"image\";s:24:\"69ced9758fcd9f2300dc.jpg\";s:8:\"position\";i:3;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:3;a:5:{s:5:\"image\";s:24:\"51b2721b4a52428b33fd.jpg\";s:8:\"position\";i:4;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:4;a:5:{s:5:\"image\";s:24:\"9dd1d90c63a7e10f416d.jpg\";s:8:\"position\";i:5;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:3;a:8:{s:4:\"name\";s:49:\"От задумки <br>до воплощения\";s:4:\"code\";s:7:\"company\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:396:\"<p>Мы имеем собственное столярное и стекольное производство, современный станочный парк. Занимаемся производством мебели из МДФ, ЛДСП и массива а также оказываем полный спектр услуг по работе со стеклом и зеркалом.</p>\";s:7:\"butName\";s:19:\"О компании\";s:7:\"butLink\";s:8:\"/company\";s:7:\"gallery\";a:2:{i:0;a:5:{s:5:\"image\";s:24:\"7768657b1926c34addc4.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"b897eadead686dad6461.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:4;a:8:{s:4:\"name\";s:14:\"Новости\";s:4:\"code\";s:4:\"news\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"butName\";s:21:\"Все новости\";s:7:\"butLink\";s:5:\"/news\";s:5:\"image\";N;s:7:\"gallery\";a:0:{}}i:5;a:8:{s:4:\"name\";s:26:\"Будьте в курсе\";s:4:\"code\";s:8:\"subcribe\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:149:\"<p>Подпишитесь на рассылку и узнавайте первыми о актуальных новостях и новинках</p>\";s:7:\"butName\";s:22:\"Подписаться\";s:7:\"butLink\";s:1:\"#\";s:7:\"gallery\";a:2:{i:0;a:5:{s:5:\"image\";s:24:\"281afc22face7fa9d515.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"0f335f217becffc18bcc.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:6;a:8:{s:4:\"name\";s:12:\"Отзывы\";s:4:\"code\";s:7:\"reviews\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"87f26c31f7ac9f00c4fa.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:7;a:8:{s:4:\"name\";s:32:\"Остались вопросы?\";s:4:\"code\";s:9:\"questions\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:199:\"<p>Позвоните нам и мы проконсультируем <br>Вас по интересующим вопросам</p>\r\n<div class=\"questions-phone\">\r\n  [[w:ContentMyBlock|id=1]]\r\n</div>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"66a7ecac6341abaa9e4c.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}}', '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('2', null, 'ru', null, '2019-01-14 09:57:27', '2020-10-30 15:17:58', '1', '1', 'Меню', 'Меню', 'menyu', '<div>[[w:SiteMap]]</div>', '', '', '1', '0', '2', '', '', '', null, null, '', '', null, '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('3', null, 'ru', null, '2019-01-14 09:57:56', '2020-10-30 13:41:48', '1', '1', 'О нас', 'О нас', 'company', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '3', '', 'view-company', '', null, null, '', '', 'a:5:{i:1;a:8:{s:4:\"name\";s:43:\"Несколько <br />слов о нас\";s:4:\"code\";s:12:\"company-box1\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:1591:\"<p>С 2013 года мы занимаемся комплексным оснащением интерьеров по индивидуальному проекту. Реализация проекта под ключ, полное оснащение мебелью и прочими элементами интерьера по индивидуальному проекту по всему миру.</p>\r\n<p>В перечень успешно реализованных нами проектов вошли предприятия разных сфер деятельности: Рестораны, столовые, фитнес центры, салоны красоты, офисы, кинотеатры, банки, аэропорты, торговые центры, гостиницы, медицинские и детские учреждения.</p>\r\n<blockquote>\r\n<p>От первого впечатления зависит оценка любого заведения, поэтому владельцам бизнеса важно заботиться не только о высоком качестве услуг, но и о дизайне помещения. Какой бы эффективной ни была задумка, важно подобрать мебель, которая будет удобной, качественной и гармоничной в выбранном варианте интерьера. Единственная в своем роде. Эксклюзивная, создающая комфорт и впечатление...</p>\r\n</blockquote>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:5:\"image\";s:24:\"1f4953cc7d3a7918c658.jpg\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"6f770bf0a80389ef1925.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}}i:2;a:8:{s:4:\"name\";s:86:\"Хороший, сильный <br>сервис, то чем мы <br>гордимся\";s:4:\"code\";s:12:\"company-box2\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:852:\"<p>Первокласное обслуживание клиентов для нас не просто слова, а целая философия. Мы верим в индивидуальный подход к каждому клиенту, и знаем, что вежливость, компетентность и профессионализм &mdash; это именно то, что создает фундамент долгосрочного сотрудничества.</p>\r\n<p>Текст о компании, который дает некоторое понимание о предприятии. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:5:\"image\";s:24:\"865486e1da3d4d2b1e5d.jpg\";s:7:\"gallery\";a:0:{}}i:3;a:8:{s:4:\"name\";s:47:\"Качественные <br>материалы\";s:4:\"code\";s:12:\"company-box3\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:400:\"<p>Текст о компании, несколько абзацев, которые дают некоторое понимание о предприятии. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>\";s:7:\"butName\";s:18:\"Портфолио\";s:7:\"butLink\";s:10:\"/portfolio\";s:7:\"gallery\";a:1:{i:0;a:5:{s:5:\"image\";s:24:\"f495dfd962dcbf5f9e18.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:4;a:8:{s:4:\"name\";s:115:\"Профессионализм <br>и внимательность — <br>гарантия нашей работы\";s:4:\"code\";s:12:\"company-box4\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:857:\"<p>Текст о компании, который дает некокоторое понимание о предприятии. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст о компании, который дает некоторое понимание о предприятии.</p>\r\n<p>Текст о компании, который дает некоторое понимание о предприятии. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>\";s:7:\"butName\";s:14:\"Магазин\";s:7:\"butLink\";s:1:\"#\";s:5:\"image\";s:24:\"b7562d36b2419d88a4a1.jpg\";s:7:\"gallery\";a:0:{}}i:5;a:8:{s:4:\"name\";s:78:\"Изображения сбоку в блоке ПРОФЕССИОНАЛИЗМ\";s:4:\"code\";s:17:\"company-box4-back\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:7:\"gallery\";a:2:{i:0;a:5:{s:5:\"image\";s:24:\"551efdd485a7ad7e26a2.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"18c8a9ce4754997632f1.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}}', '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('4', null, 'ru', null, '2019-01-14 09:58:14', '2020-10-30 13:41:48', '1', '1', 'MIRRION', 'Портфолио', 'portfolio', '<div>[[w:Directions|id=4;view=direction-page-widget]]</div>', '', '', '1', '0', '4', '', '', '', null, null, '', 'MIRRION', null, '', '', null, null, null);
INSERT INTO `yupe_page_page` VALUES ('5', null, 'ru', null, '2019-01-14 09:58:29', '2020-10-30 13:41:48', '1', '1', 'Магазин', 'Магазин', 'magazin', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '5', '', '', '', null, null, '', '', null, null, null, null, null, null);
INSERT INTO `yupe_page_page` VALUES ('6', null, 'ru', null, '2019-01-14 09:58:44', '2020-10-30 13:41:48', '1', '1', 'Контакты', 'Контакты', 'kontakty', '<div>\r\n<div><strong>Адрес:</strong></div>\r\n<div class=\"contact-page-text\">[[w:ContentMyBlock|id=3]]<br /><br /></div>\r\n</div>\r\n<div>\r\n<div><strong>Телефон:</strong></div>\r\n[[w:ContentMyBlock|id=1]]<br /><br /></div>\r\n<div>\r\n<div><strong>E-mail:</strong></div>\r\n[[w:ContentMyBlock|id=2]]<br /><br /></div>\r\n<div>\r\n<div><strong>График работы:</strong></div>\r\n[[w:ContentMyBlock|id=7]]<br /><br /></div>\r\n<div>\r\n<div><strong>Социальные сети:</strong></div>\r\n[[w:NewGallery|id=2;view=view-soc]]</div>', '', '', '1', '0', '6', '', 'view-contact', '', null, null, '', '', null, '', '', null, null, null);
INSERT INTO `yupe_page_page` VALUES ('7', null, 'ru', '10', '2019-01-14 09:58:58', '2020-10-30 13:41:48', '1', '1', 'Венецианские', 'Венецианские', 'venecianskie', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '38', '', 'portfolio-case-type2', '', '0938f3c4346d86fa9eade76579785010.jpg', null, '', '', null, '', '', '38', '39', '');
INSERT INTO `yupe_page_page` VALUES ('8', null, 'ru', '4', '2020-10-15 12:17:49', '2020-10-30 13:41:48', '1', '1', 'COMMERCIAL', 'MIRRION COMMERCIAL', 'mirrion-commercial', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '7', '', 'portfolio-main-type1', '', null, '59c57078d97546cd24d4edaa8fb0dca3.png', '<p>MIRRION - незаменимый помощник для воплощения бизнес-проектов. Собственная производственная площадка состоит из столярного, сварочного, стекольно-зеркального цехов. Прекрасный сервис по проектированию, производству и монтажуизделий позволяет реализовывать проекты в срок, в полном соответствии с идеями.</p>', '', null, 'Для общественнных интерьеров', '<svg width=\"14\" height=\"15\" viewBox=\"0 0 14 15\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n<path d=\"M0.655727 0L0.472438 0.181156C0.472438 0.181156 -0.195405 2.57053 6.18477 8.95071C7.67811 8.10127 8.46474 9.87362 8.53822 10.1257C8.86339 10.4504 12.2248 13.7623 12.2248 13.7623L12.2272 13.7576C12.2293 13.7623 12.2314 13.7666 12.2359 13.7705C12.5412 14.0781 13.0379 14.0781 13.3411 13.7705C13.6464 13.4671 13.6464 12.9689 13.3411 12.6659C13.3389 12.6612 13.3344 12.657 13.3306 12.657V12.6527C10.0647 9.38661 10.0666 9.38488 9.19749 8.52176C8.85717 8.17905 0.655727 0 0.655727 0Z\" fill=\"#101010\"/>\r\n<path d=\"M14.0009 2.10996L13.677 1.78479L10.3766 5.27846L9.98738 4.88772L13.3839 1.49125L13.0988 1.20752L9.681 4.62514L9.35841 4.30232L12.7758 0.884266L12.4618 0.570645L9.04394 3.98826L8.66107 3.60543L12.1892 0.296116L11.893 0L8.64143 2.5321C8.64143 2.5321 8.5235 2.63187 8.4829 2.67288C8.03681 3.11874 7.84495 3.72444 7.90865 4.30659C7.77188 6.57439 5.81752 7.07409 5.52655 7.36506C5.2048 7.68854 0.239344 12.6536 0.239344 12.6536L0.24019 12.6544C0.236365 12.6587 0.232063 12.6595 0.227797 12.6638C-0.07641 12.968 -0.0756009 13.462 0.228238 13.7658C0.531599 14.07 1.02682 14.07 1.32977 13.7666C1.33404 13.7619 1.33617 13.7576 1.33915 13.7542L1.34 13.755C1.34 13.755 6.24434 8.85241 6.61864 8.47766C6.9562 8.13885 7.75176 6.11289 9.82755 6.09175C9.82969 6.0909 9.83524 6.09005 9.83822 6.09005C10.3266 6.10373 10.8193 5.94177 11.2119 5.5987C11.2397 5.57431 11.3034 5.50852 11.3034 5.50852L14.0009 2.10996Z\" fill=\"#101010\"/>\r\n</svg>', '12', '9', '');
INSERT INTO `yupe_page_page` VALUES ('9', null, 'ru', '4', '2020-10-15 12:18:41', '2020-10-30 13:41:48', '1', '1', 'HOME', 'MIRRION HOME', 'mirrion-home', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '8', '', 'portfolio-main-type1', '', null, '5b86c598500f01c65c800978139f7343.png', '', '', null, 'Для домашнего интерьера', '<svg width=\"13\" height=\"13\" viewBox=\"0 0 13 13\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n<path d=\"M10.2917 12.9997H2.70833C2.11088 12.9997 1.625 12.5139 1.625 11.9164V7.0414H0.541667C0.237792 7.0414 0 6.8036 0 6.49973C0 6.35619 0.0585 6.21535 0.160333 6.11352L6.31042 0.0771875C6.4155 -0.0257292 6.58396 -0.0257292 6.69012 0.0771875L8.66667 2.01906V0.812229C8.66667 0.662729 8.788 0.541396 8.9375 0.541396H11.1042C11.2537 0.541396 11.375 0.662729 11.375 0.812229V4.67431L12.838 6.11135C12.9415 6.21535 13 6.35619 13 6.49973C13 6.8036 12.7622 7.0414 12.4583 7.0414H11.375V11.9164C11.375 12.5139 10.8891 12.9997 10.2917 12.9997ZM6.5 0.650271L0.541667 6.4981L1.89583 6.49973C2.04533 6.49973 2.16667 6.62106 2.16667 6.77056V11.9164C2.16667 12.2149 2.40988 12.4581 2.70833 12.4581H10.2917C10.5901 12.4581 10.8333 12.2149 10.8333 11.9164V6.77056C10.8333 6.62106 10.9547 6.49973 11.1042 6.49973H12.4583L12.4567 6.49648L10.9146 4.98144C10.8626 4.93052 10.8333 4.86065 10.8333 4.78806V1.08306H9.20833V2.66473C9.20833 2.77415 9.14279 2.87219 9.04204 2.91444C8.94238 2.95723 8.82592 2.93448 8.74738 2.8581L6.5 0.650271Z\" fill=\"#101010\"/>\r\n</svg>', '8', '10', null);
INSERT INTO `yupe_page_page` VALUES ('10', null, 'ru', '4', '2020-10-15 12:19:13', '2020-10-30 13:41:48', '1', '1', 'MIRRORS', 'MIRRION MIRRORS', 'mirrion-mirrors', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '9', '', 'portfolio-main-type2', '', null, '8fc709085a044ac52eb521eb5932c426.png', '', '', null, 'Зеркала, стекло', '<svg width=\"17\" height=\"17\" viewBox=\"0 0 17 17\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n<g clip-path=\"url(#clip0)\">\r\n<path d=\"M8.50001 14.6818C5.94342 14.6818 3.86365 11.9085 3.86365 8.49993C3.86365 5.09133 5.94342 2.31812 8.50001 2.31812C11.0566 2.31812 13.1364 5.09133 13.1364 8.49993C13.1364 11.9085 11.0566 14.6818 8.50001 14.6818ZM8.50001 2.75967C6.17571 2.75967 4.28483 5.33464 4.28483 8.49993C4.28483 11.6652 6.17571 14.2402 8.50001 14.2402C10.8243 14.2402 12.7152 11.6652 12.7152 8.49993C12.7152 5.33464 10.8243 2.75967 8.50001 2.75967Z\" fill=\"#101010\"/>\r\n<path d=\"M8.50637 16.9958C4.697 16.9958 1.59756 13.1837 1.59756 8.49788C1.59756 3.81209 4.697 0 8.50637 0C12.3159 0 15.4152 3.81209 15.4152 8.49788C15.4152 13.1837 12.3159 16.9958 8.50637 16.9958ZM8.50637 0.531117C4.98992 0.531117 2.12868 4.10501 2.12868 8.49788C2.12868 12.8907 4.98953 16.4646 8.50637 16.4646C12.0232 16.4646 14.8841 12.8907 14.8841 8.49788C14.8841 4.10501 12.0229 0.531117 8.50637 0.531117Z\" fill=\"#101010\"/>\r\n<path d=\"M6.02606 3.61009C5.93425 3.61009 5.84504 3.56263 5.7959 3.47731L4.41203 1.93806C4.33877 1.81085 4.38233 1.64864 4.50928 1.57538C4.63596 1.50173 4.79844 1.54529 4.87196 1.6725L6.25583 3.21175C6.32909 3.33896 6.28552 3.5013 6.15858 3.57456C6.11695 3.59868 6.07131 3.61009 6.02606 3.61009Z\" fill=\"#101010\"/>\r\n<path d=\"M12.3711 15.4563C12.2791 15.4563 12.1899 15.4085 12.1408 15.3235L10.7445 13.7883C10.6711 13.6611 10.7147 13.4987 10.8416 13.4255C10.9683 13.3522 11.1308 13.3958 11.2044 13.5227L12.6007 15.058C12.674 15.1852 12.6305 15.3474 12.5036 15.4207C12.4618 15.4449 12.4162 15.4563 12.3711 15.4563Z\" fill=\"#101010\"/>\r\n<path d=\"M4.38206 6.21762C4.33694 6.21762 4.2913 6.20621 4.24954 6.18196L2.3488 5.24942C2.22159 5.17616 2.17803 5.01329 2.25168 4.88661C2.32494 4.75941 2.48767 4.71584 2.61436 4.78949L4.5151 5.72203C4.6423 5.79542 4.68587 5.95816 4.61235 6.08484C4.56321 6.17003 4.474 6.21762 4.38206 6.21762Z\" fill=\"#101010\"/>\r\n<path d=\"M14.5275 12.2395C14.4824 12.2395 14.4368 12.2281 14.3952 12.204L12.4842 11.2781C12.357 11.2048 12.3134 11.0423 12.3869 10.9154C12.4605 10.7885 12.6228 10.7446 12.7497 10.8181L14.6607 11.7441C14.7878 11.8174 14.8314 11.9798 14.7578 12.1068C14.7084 12.1918 14.6195 12.2395 14.5275 12.2395Z\" fill=\"#101010\"/>\r\n<path d=\"M2.487 12.2384C2.3952 12.2384 2.30586 12.1906 2.25685 12.1056C2.18345 11.9784 2.22702 11.8162 2.35397 11.743L4.27316 10.721C4.39984 10.6476 4.56231 10.6912 4.63596 10.8181C4.70923 10.9453 4.66566 11.1075 4.53871 11.1809L2.61952 12.2029C2.5779 12.227 2.53213 12.2384 2.487 12.2384Z\" fill=\"#101010\"/>\r\n<path d=\"M12.6177 6.21727C12.5258 6.21727 12.4365 6.16982 12.3874 6.0845C12.3141 5.95755 12.3577 5.79508 12.4847 5.72182L14.3949 4.79112C14.5214 4.71812 14.684 4.76117 14.7575 4.88837C14.8308 5.01531 14.7872 5.17779 14.6604 5.25105L12.7502 6.18175C12.7085 6.20586 12.6628 6.21727 12.6177 6.21727Z\" fill=\"#101010\"/>\r\n<path d=\"M4.64222 15.4565C4.59709 15.4565 4.55158 15.4451 4.5097 15.4208C4.38275 15.3478 4.33893 15.1851 4.41245 15.0581L5.79545 13.5229C5.86871 13.3957 6.03041 13.3521 6.15813 13.4256C6.28507 13.4986 6.3289 13.6615 6.25538 13.7885L4.87238 15.3237C4.82323 15.4089 4.73402 15.4565 4.64222 15.4565Z\" fill=\"#101010\"/>\r\n<path d=\"M10.9738 3.61014C10.9287 3.61014 10.8831 3.59872 10.8413 3.57448C10.7141 3.50121 10.6705 3.33874 10.7442 3.2118L12.1364 1.67926C12.2098 1.55205 12.3723 1.50874 12.4992 1.58201C12.6264 1.65527 12.67 1.81787 12.5963 1.94482L11.2041 3.47736C11.155 3.56255 11.0658 3.61014 10.9738 3.61014Z\" fill=\"#101010\"/>\r\n<path d=\"M2.40285 9.29892C1.96366 9.29892 1.60617 8.94143 1.60617 8.50224C1.60617 8.06293 1.96366 7.70557 2.40285 7.70557C2.84203 7.70557 3.19952 8.06293 3.19952 8.50224C3.19952 8.94143 2.84203 9.29892 2.40285 9.29892ZM2.40285 8.23668C2.25619 8.23668 2.13729 8.35559 2.13729 8.50224C2.13729 8.64877 2.25619 8.7678 2.40285 8.7678C2.54937 8.7678 2.6684 8.64877 2.6684 8.50224C2.6684 8.35559 2.54911 8.23668 2.40285 8.23668Z\" fill=\"#101010\"/>\r\n<path d=\"M14.6182 9.29892C14.179 9.29892 13.8215 8.94143 13.8215 8.50224C13.8215 8.06293 14.179 7.70557 14.6182 7.70557C15.0574 7.70557 15.4149 8.06293 15.4149 8.50224C15.4149 8.94143 15.0574 9.29892 14.6182 9.29892ZM14.6182 8.23668C14.4716 8.23668 14.3527 8.35559 14.3527 8.50224C14.3527 8.64877 14.4716 8.7678 14.6182 8.7678C14.7647 8.7678 14.8838 8.64877 14.8838 8.50224C14.8838 8.35559 14.7645 8.23668 14.6182 8.23668Z\" fill=\"#101010\"/>\r\n<path d=\"M8.51082 1.59775C8.07164 1.59775 7.71414 1.24025 7.71414 0.80107C7.71414 0.361758 8.07164 0.00439453 8.51082 0.00439453C8.95 0.00439453 9.3075 0.361758 9.3075 0.80107C9.3075 1.24025 8.95 1.59775 8.51082 1.59775ZM8.51082 0.535512C8.36417 0.535512 8.24526 0.654417 8.24526 0.80107C8.24526 0.947594 8.36417 1.06663 8.51082 1.06663C8.65734 1.06663 8.77638 0.947594 8.77638 0.80107C8.77638 0.654417 8.65708 0.535512 8.51082 0.535512Z\" fill=\"#101010\"/>\r\n<path d=\"M8.51082 17.0001C8.07164 17.0001 7.71414 16.6426 7.71414 16.2034C7.71414 15.7641 8.07164 15.4067 8.51082 15.4067C8.95 15.4067 9.3075 15.7641 9.3075 16.2034C9.3075 16.6426 8.95 17.0001 8.51082 17.0001ZM8.51082 15.9379C8.36417 15.9379 8.24526 16.0568 8.24526 16.2034C8.24526 16.3499 8.36417 16.469 8.51082 16.469C8.65734 16.469 8.77638 16.3499 8.77638 16.2034C8.77638 16.0568 8.65708 15.9379 8.51082 15.9379Z\" fill=\"#101010\"/>\r\n</g>\r\n<defs>\r\n<clipPath id=\"clip0\">\r\n<rect width=\"17\" height=\"17\" fill=\"white\"/>\r\n</clipPath>\r\n</defs>\r\n</svg>\r\n', '9', '11', '');
INSERT INTO `yupe_page_page` VALUES ('11', null, 'ru', '4', '2020-10-15 12:20:11', '2020-10-30 13:41:48', '1', '1', 'Металлоизделия', 'MIRRION металлоизделия', 'mirrion-metalloizdeliya', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '10', '', 'portfolio-main-type1', '', null, 'c1471ad3cf88a8eda343caa06401fa1e.png', '', '', null, 'Металлоизделия', '', '10', '12', null);
INSERT INTO `yupe_page_page` VALUES ('12', null, 'ru', '4', '2020-10-15 12:20:37', '2020-10-30 13:41:48', '1', '1', 'Изделия из дерева', 'MIRRION изделия из дерева', 'mirrion-izdeliya-iz-dereva', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '11', '', 'portfolio-main-type1', '', null, '422b4e762382f88936482666db80041a.png', '', '', null, 'Изделия из дерева', '', '11', '8', null);
INSERT INTO `yupe_page_page` VALUES ('13', null, 'ru', '8', '2020-10-19 17:45:01', '2020-10-30 13:41:48', '1', '1', 'Рестораны', 'MIRRION COMMERCIAL Рестораны', 'restorany', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '12', '', 'portfolio-case-type1', '', null, null, '', '', null, '', '', '16', '14', 'Рестораны');
INSERT INTO `yupe_page_page` VALUES ('14', null, 'ru', '8', '2020-10-19 17:46:00', '2020-10-30 13:41:48', '1', '1', 'Магазины', 'MIRRION COMMERCIAL Магазины', 'magaziny', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '13', '', 'portfolio-case-type1', '', null, null, '', '', null, '', '', '13', '15', 'Магазины');
INSERT INTO `yupe_page_page` VALUES ('15', null, 'ru', '8', '2020-10-19 17:46:33', '2020-10-30 13:41:48', '1', '1', 'Офисы', 'MIRRION COMMERCIAL Офисы', 'ofisy', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '14', '', 'portfolio-case-type1', '', null, null, '', '', null, '', '', '14', '16', 'Офисы');
INSERT INTO `yupe_page_page` VALUES ('16', null, 'ru', '8', '2020-10-19 17:47:13', '2020-10-30 13:41:48', '1', '1', 'Панели', 'MIRRION COMMERCIAL Панели', 'paneli', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '15', '', 'portfolio-case-type1', '', null, null, '', '', null, '', '', '15', '13', 'Панели');
INSERT INTO `yupe_page_page` VALUES ('17', null, 'ru', '13', '2020-10-20 15:26:21', '2020-10-30 13:41:48', '1', '1', 'Eclipse', 'Ресторан Eclipse г. Санкт-Петербург', 'restoran-eclipse-g-sankt-peterburg', '<p><strong>ОБЪЕКТ: </strong> <br />Ресторан ECLIPSE <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '16', '', 'portfolio-view-type1', '', 'd56d38e4b96448d65e98a125451836b5.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', 'a:5:{i:1;a:8:{s:4:\"name\";s:33:\"Концепция проекта\";s:4:\"code\";s:4:\"box1\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:1014:\"<p>Ресторан ECLIPSE - один из самых красивых ресторанов, открытых в окрестностях Санкт-Петербурга был задуман как городской ресторан за городом. Главным пожеланием заказчика было создать идеальный, яркий, стильный интерьер с насыщенной контрастной цветовой гаммой и деталями, выверенными до мелочей.</p>\r\n<p>Дизайн-решением данной задачи занималась одна из самых титулованных архитектурных студий SUNDUKOVY SISTERS, известные нестандартным подходом к своим дизайн-проектам. Каждая мельчайшая деталь в проекте была тщательно продумана, чтобы достичь совершенства.</p>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:7:\"gallery\";a:2:{i:0;a:5:{s:5:\"image\";s:24:\"c3425d1893390d90d01b.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"8c6a179c6c6c7c083fbb.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:2;a:8:{s:4:\"name\";s:12:\"Панели\";s:4:\"code\";s:4:\"box2\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:652:\"<p>Феерия сверкающих зеркал, панелей, металла в проекте нового ресторана Eclipse от @sundukovy_sisters.</p>\r\n<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:7:\"gallery\";a:2:{i:0;a:5:{s:5:\"image\";s:24:\"11a2f0b76c57076ede04.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"f9fd03c0dd97e39f82f7.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:3;a:8:{s:4:\"name\";s:25:\"Скрытые Двери\";s:4:\"code\";s:4:\"box3\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:875:\"<p>Скрытые двери в составе стеновых панелей оформлены розовым зеркалом в сочетании с рамками в ардеко-орнаменте.</p>\r\n<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:7:\"gallery\";a:2:{i:0;a:5:{s:5:\"image\";s:24:\"287351ac1bdac224e13b.jpg\";s:8:\"position\";i:1;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}i:1;a:5:{s:5:\"image\";s:24:\"cabd57f277ec69f6cdc1.jpg\";s:8:\"position\";i:2;s:5:\"title\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:5:\"image\";N;}i:4;a:8:{s:4:\"name\";s:16:\"Ресепшен\";s:4:\"code\";s:4:\"box4\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:708:\"<p>Феерия сверкающих зеркал, панелей, металла в проекте нового ресторана Eclipse от @sundukovy_sisters. Скрытые двери в составе стеновых панелей оформлены розовым зеркалом в сочетании с рамками в ардеко-орнаменте.</p>\r\n<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:5:\"image\";s:24:\"c590a5ef248fea233833.png\";s:7:\"gallery\";a:0:{}}i:5;a:8:{s:4:\"name\";s:27:\"Детали проекта\";s:4:\"code\";s:4:\"box5\";s:8:\"template\";s:8:\"Redactor\";s:5:\"value\";s:1398:\"<p>В процессе реализации проекта заказчик несколько раз вносил свои изменения и корректировки в сторону улучшения и совершенствования. Под каждое из дополнений наша команда делала сигнальные образцы перед тем, как воплощать задуманные идеи в уже готовое решение, что позволило нам полностью реализовать желание заказчика и не помешало сдать роскошный проект в оговоренные сроки. В процессе реализации проекта заказчик несколько раз вносил свои изменения и корректировки в сторону улучшения и совершенствования. Под каждое из дополнений наша команда делала сигнальные образцы перед тем, как воплощать задуманные идеи в уже готовое решение, что позволило нам полностью реализовать желание заказчика и не помешало сдать роскошный проект в оговоренные сроки.</p>\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:0:\"\";s:5:\"image\";N;s:7:\"gallery\";a:4:{i:0;a:2:{s:5:\"image\";s:24:\"1338e4be76ef48468794.jpg\";s:8:\"position\";i:1;}i:1;a:2:{s:5:\"image\";s:24:\"029ee7085f718f296bbb.jpg\";s:8:\"position\";i:2;}i:2;a:2:{s:5:\"image\";s:24:\"c08a9534caef6e7a3e3d.jpg\";s:8:\"position\";i:3;}i:3;a:2:{s:5:\"image\";s:24:\"130aa42e9f51d8e02911.jpg\";s:8:\"position\";i:4;}}}}', '', '', '21', '18', 'Eclipse');
INSERT INTO `yupe_page_page` VALUES ('18', null, 'ru', '13', '2020-10-20 15:30:38', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Ресторан Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg', '<p><strong>ОБЪЕКТ: </strong> <br />Ресторан ECLIPSE <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '17', '', 'portfolio-view-type1', '', 'bff249a849343e65a5dad9c916dc0180.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '17', '19', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('19', null, 'ru', '13', '2020-10-20 15:35:18', '2020-10-30 13:41:48', '1', '1', 'Eclipse', 'Ресторан Eclipse г. Санкт-Петербург', 'restoran-eclipse-g-sankt-peterburg-2', '<p><strong>ОБЪЕКТ: </strong> <br />Ресторан ECLIPSE <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '18', '', 'portfolio-view-type1', '', 'e8e57f4d851221c2cf8d0b97e6c7f6ac.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '18', '20', 'Eclipse');
INSERT INTO `yupe_page_page` VALUES ('20', null, 'ru', '13', '2020-10-20 15:36:22', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Ресторан Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg-2', '<p><strong>ОБЪЕКТ: </strong> <br />Ресторан ECLIPSE <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '19', '', 'portfolio-view-type1', '', '59352aec94d32456af8e466020fee7be.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '19', '21', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('21', null, 'ru', '13', '2020-10-20 15:38:39', '2020-10-30 13:41:48', '1', '1', 'Eclipse', 'Ресторан Eclipse г. Санкт-Петербург', 'restoran-eclipse-g-sankt-peterburg-3', '<p><strong>ОБЪЕКТ: </strong> <br />Ресторан ECLIPSE <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '20', '', 'portfolio-view-type1', '', 'd18e280abb868afca7cf10a1722f58f7.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '20', '17', 'Eclipse');
INSERT INTO `yupe_page_page` VALUES ('22', null, 'ru', '14', '2020-10-20 15:26:21', '2020-10-30 13:41:48', '1', '1', 'Бутик Free Age', 'Бутик Free Age г. Санкт-Петербург', 'restoran-free-age-g-sankt-peterburg-14', '<p><strong>ОБЪЕКТ: </strong> <br />Бутик Free Age <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '21', '', 'portfolio-view-type1', '', 'be11b9d2e2a393b48a60a3554a1d701d.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '26', '23', 'Бутик Free Age');
INSERT INTO `yupe_page_page` VALUES ('23', null, 'ru', '14', '2020-10-20 15:30:38', '2020-10-30 13:41:48', '1', '1', 'ДЛТ г. Санкт-Петербург', 'ДЛТ г. Санкт-Петербург', 'restoran-dlt-g-sankt-peterburg-14', '<p><strong>ОБЪЕКТ: </strong> <br />Бутик Free Age <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '22', '', 'portfolio-view-type1', '', '6eda0647e2219e1241d86d605ed2e21e.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '22', '24', 'ДЛТ');
INSERT INTO `yupe_page_page` VALUES ('24', null, 'ru', '14', '2020-10-20 15:35:18', '2020-10-30 13:41:48', '1', '1', 'Бутик Free Age', 'Бутик Free Age г. Санкт-Петербург', 'restoran-free-age-g-sankt-peterburg-14-2', '<p><strong>ОБЪЕКТ: </strong> <br />Бутик Free Age <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '23', '', 'portfolio-view-type1', '', 'a04d5a1cd2dca373384e3c5c7c382a6f.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '23', '25', 'Бутик Free Age');
INSERT INTO `yupe_page_page` VALUES ('25', null, 'ru', '14', '2020-10-20 15:36:22', '2020-10-30 13:41:48', '1', '1', 'ДЛТ г. Санкт-Петербург', 'ДЛТ г. Санкт-Петербург', 'restoran-dlt-g-sankt-peterburg-14-2', '<p><strong>ОБЪЕКТ: </strong> <br />Бутик Free Age <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '24', '', 'portfolio-view-type1', '', '6b382b77aa431d1ba4e1da6bf1975fec.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '24', '26', 'ДЛТ');
INSERT INTO `yupe_page_page` VALUES ('26', null, 'ru', '14', '2020-10-20 15:38:39', '2020-10-30 13:41:48', '1', '1', 'Бутик Free Age', 'Бутик Free Age г. Санкт-Петербург', 'restoran-free-age-g-sankt-peterburg-14-3', '<p><strong>ОБЪЕКТ: </strong> <br />Бутик Free Age <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '25', '', 'portfolio-view-type1', '', '7d7c0b7912f7ee72b1a75fbd221d6b36.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '25', '21', 'Бутик Free Age');
INSERT INTO `yupe_page_page` VALUES ('27', null, 'ru', '15', '2020-10-20 15:26:21', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Офис Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg-15', '<p><strong>ОБЪЕКТ: </strong> <br />Офис Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '26', '', 'portfolio-view-type1', '', '6d24184f363e7350b10c0202e38715e9.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '31', '28', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('28', null, 'ru', '15', '2020-10-20 15:30:38', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Офис Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg-15-2', '<p><strong>ОБЪЕКТ: </strong> <br />Офис Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '27', '', 'portfolio-view-type1', '', 'f3d0a7366f780c28f9487a11b312e633.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '27', '29', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('29', null, 'ru', '15', '2020-10-20 15:35:18', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Офис Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg-15-3', '<p><strong>ОБЪЕКТ: </strong> <br />Офис Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '28', '', 'portfolio-view-type1', '', '888b76c8dc86989807197b3f3000452f.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '28', '30', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('30', null, 'ru', '15', '2020-10-20 15:36:22', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Офис Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg-15-4', '<p><strong>ОБЪЕКТ: </strong> <br />Офис Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '29', '', 'portfolio-view-type1', '', '825305b4fd43022f42051240b7d1f538.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '29', '31', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('31', null, 'ru', '15', '2020-10-20 15:38:39', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Офис Jetbrains г. Санкт-Петербург', 'restoran-jetbrains-g-sankt-peterburg-15-5', '<p><strong>ОБЪЕКТ: </strong> <br />Офис Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '30', '', 'portfolio-view-type1', '', '3237171240226d725733f591e2400f38.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '30', '27', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('32', null, 'ru', '16', '2020-10-20 15:26:21', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Панель Jetbrains г. Санкт-Петербург', 'panel-jetbrains-g-sankt-peterburg-16', '<p><strong>ОБЪЕКТ: </strong> <br />Панель Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '31', '', 'portfolio-view-type1', '', '633ca83e03ce4a13b7cdc51d96d52233.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '36', '33', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('33', null, 'ru', '16', '2020-10-20 15:30:38', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Панель Jetbrains г. Санкт-Петербург', 'panel-jetbrains-g-sankt-peterburg-16-2', '<p><strong>ОБЪЕКТ: </strong> <br />Панель Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '32', '', 'portfolio-view-type1', '', 'ceee6c3bacad520e4f81374887973b6e.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '32', '34', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('34', null, 'ru', '16', '2020-10-20 15:35:18', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Панель Jetbrains г. Санкт-Петербург', 'panel-jetbrains-g-sankt-peterburg-16-3', '<p><strong>ОБЪЕКТ: </strong> <br />Панель Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '33', '', 'portfolio-view-type1', '', 'f2d33048252b7045ed7f223ffcfa2cf2.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '33', '35', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('35', null, 'ru', '16', '2020-10-20 15:36:22', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Панель Jetbrains г. Санкт-Петербург', 'panel-jetbrains-g-sankt-peterburg-16-4', '<p><strong>ОБЪЕКТ: </strong> <br />Панель Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '34', '', 'portfolio-view-type1', '', '762b392468f91586d41dcc64a6defb5f.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '34', '36', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('36', null, 'ru', '16', '2020-10-20 15:38:39', '2020-10-30 13:41:48', '1', '1', 'Jetbrains', 'Панель Jetbrains г. Санкт-Петербург', 'panel-jetbrains-g-sankt-peterburg-16-5', '<p><strong>ОБЪЕКТ: </strong> <br />Панель Jetbrains <br />в г. Санкт-Петербург</p>\r\n<p><strong>ОБЪЕМ РАБОТ: </strong> <br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', '', '1', '0', '35', '', 'portfolio-view-type1', '', '0c327f427fd0f387b67b71231f0de3d9.jpg', null, '<p><strong>ОБЪЕМ РАБОТ: </strong><br />Проектировка, производство и монтаж оборудования ресепшен, столовой и санузлов.</p>\r\n<p><strong>МАТЕРИАЛЫ: </strong> <br />Металл, дерево, акрил, стекло</p>', '', null, '', '', '35', '32', 'Jetbrains');
INSERT INTO `yupe_page_page` VALUES ('37', null, 'ru', '10', '2020-10-22 11:45:50', '2020-10-30 13:41:48', '1', '1', 'Пано', 'Пано', 'pano', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '36', '', 'portfolio-case-type2', '', '58eb214e5cb0c136fb5bed1bcd4ef549.jpg', null, '', '', null, '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('38', null, 'ru', '10', '2020-10-22 11:58:46', '2020-10-30 13:41:48', '1', '1', 'Состаренные', 'Состаренные', 'sostarennye', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '37', '', 'portfolio-case-type2', '', 'a31401770f3f5f6a81471bc634c9196b.jpg', null, '', '', null, '', '', '37', '7', '');
INSERT INTO `yupe_page_page` VALUES ('39', null, 'ru', '10', '2020-10-22 12:01:58', '2020-10-30 13:41:48', '1', '1', 'С подсветкой', 'С подсветкой', 's-podsvetkoy', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '39', '', 'portfolio-case-type2', '', 'ba1bef1aaad960b5eaf1f13ba0c6b35c.jpg', null, '', '', null, '', '', '38', '40', '');
INSERT INTO `yupe_page_page` VALUES ('40', null, 'ru', '10', '2020-10-22 12:03:02', '2020-10-30 13:41:48', '1', '1', 'Стеклянные перегородки', 'Стеклянные перегородки', 'steklyannye-peregorodki', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '40', '', 'portfolio-case-type2', '', '5c67ec0720420b6839f58e79d3152248.jpg', null, '', '', null, '', '', '39', '42', '');
INSERT INTO `yupe_page_page` VALUES ('41', null, 'ru', '10', '2020-10-22 12:03:42', '2020-10-30 13:41:48', '1', '1', 'Окрашенное стекло', 'Окрашенное стекло', 'okrashennoe-steklo', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '41', '', 'portfolio-case-type2', '', '5ff53c3524aa29fdc58154a2717e1603.jpg', null, '', '', null, '', '', '40', null, '');
INSERT INTO `yupe_page_page` VALUES ('42', null, 'ru', '10', '2020-10-22 12:04:51', '2020-10-30 13:41:48', '1', '1', 'Интерьерные зеркала', 'Интерьерные зеркала', 'interernye-zerkala', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '42', '', 'portfolio-case-type2', '', '9d4b212a0aa4cf62b1bc3ee02de76cf9.jpg', null, '', '', null, '', '', '41', '7', '');
INSERT INTO `yupe_page_page` VALUES ('43', null, 'ru', '37', '2020-10-22 13:06:45', '2020-10-30 13:41:48', '1', '1', '', 'Пано 1', 'pano-1', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '43', '', '', '', null, null, '<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\r\n<p>Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>', '', null, '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('44', null, 'ru', '37', '2020-10-22 13:07:56', '2020-10-30 13:41:48', '1', '1', '', 'Пано 2', 'pano-2', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '44', '', '', '', null, null, '<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\r\n<p>Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>', '', null, '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('45', null, 'ru', '37', '2020-10-22 13:08:54', '2020-10-30 13:41:48', '1', '1', '', 'Пано 3', 'pano-3', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '45', '', '', '', null, null, '<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\r\n<p>Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>', '', null, '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('46', null, 'ru', '37', '2020-10-22 13:09:26', '2020-10-30 13:41:48', '1', '1', '', 'Пано 4', 'pano-4', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '46', '', '', '', null, null, '<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\r\n<p>Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>', '', null, '', '', null, null, '');
INSERT INTO `yupe_page_page` VALUES ('47', null, 'ru', '37', '2020-10-22 13:09:57', '2020-10-30 13:41:48', '1', '1', '', 'Пано 5', 'pano-5', '<p>Раздел в стадии наполнения!</p>', '', '', '1', '0', '47', '', '', '', null, null, '<p>Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим.</p>\r\n<p>Необходим для примера и наглядности отображения информации на странице макета. Текст тестовый, в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета.</p>', '', null, '', '', null, null, '');

-- ----------------------------
-- Table structure for `yupe_page_page_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page_image`;
CREATE TABLE `yupe_page_page_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page_image
-- ----------------------------
INSERT INTO `yupe_page_page_image` VALUES ('1', '8', '07520b92fde04dda77412d34cc245e8e.jpg', '', '', '1');
INSERT INTO `yupe_page_page_image` VALUES ('2', '17', '9cc0f25a2f6feed836644f7bb4136040.jpg', '', '', '2');
INSERT INTO `yupe_page_page_image` VALUES ('3', '10', '764c5e33db9e96ff197293a42b2efed6.jpg', '', '', '3');
INSERT INTO `yupe_page_page_image` VALUES ('4', null, '99a2df471bc3bcaeb0c0d30aa70abf51.jpg', '', '', '4');
INSERT INTO `yupe_page_page_image` VALUES ('5', null, 'dfba1c7e5bc964455b05ea406a52a4cf.jpg', '', '', '5');
INSERT INTO `yupe_page_page_image` VALUES ('6', null, 'f15f06d6fb706cae6d6533a908f145c2.jpg', '', '', '6');
INSERT INTO `yupe_page_page_image` VALUES ('7', null, '98edada0ee166c467ab4aa18dbe93c10.jpg', '', '', '7');
INSERT INTO `yupe_page_page_image` VALUES ('8', null, '6871e987304448ed399e7531d41ee327.jpg', '', '', '8');
INSERT INTO `yupe_page_page_image` VALUES ('9', '43', 'ebfe9f5d74954d1ed866e4f54d40b7da.jpg', '', '', '9');
INSERT INTO `yupe_page_page_image` VALUES ('10', '44', 'd7914dd969264c66389de7b63214ac36.jpg', '', '', '10');
INSERT INTO `yupe_page_page_image` VALUES ('11', '45', 'e0809282996c99bb0a4f12946a71d2b7.jpg', '', '', '11');
INSERT INTO `yupe_page_page_image` VALUES ('12', '46', 'dec9bbaab96c447b77a0b3835fcda5f4.jpg', '', '', '12');
INSERT INTO `yupe_page_page_image` VALUES ('13', '47', '996c720fd63eacd3f788fd93132c4fb2.jpg', '', '', '13');
INSERT INTO `yupe_page_page_image` VALUES ('14', '45', '06ef0e1658607dee19c5750ad4de98ea.jpg', '', '', '14');
INSERT INTO `yupe_page_page_image` VALUES ('15', '44', 'f48b7149b46441d304efe67f4b160df0.jpg', '', '', '15');
INSERT INTO `yupe_page_page_image` VALUES ('16', '8', 'b137054c00e95f0bd6a93b4a9235fd65.jpg', '', '', '16');

-- ----------------------------
-- Table structure for `yupe_review`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_review`;
CREATE TABLE `yupe_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `text` text NOT NULL,
  `moderation` int(11) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `useremail` varchar(256) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `product_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `countImage` int(11) DEFAULT NULL,
  `name_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_review
-- ----------------------------
INSERT INTO `yupe_review` VALUES ('1', '1', '2020-10-16 14:37:10', 'Мы были восхищены уровнем работы и сроками изготовления. Впечатления о сотрудничестве остались только положительные. Мы были восхищены уровнем работы и сроками изготовления. Впечатления о сотрудничестве остались только положительные. Мы были восхищены уровнем работы и сроками изготовления. ', '1', 'Александра Вострикова', 'b1fcf23a022e9269de1726856121ac36.jpg', null, null, '1', null, '4', '0', 'Владелица загородного дома');
INSERT INTO `yupe_review` VALUES ('2', '1', '2020-10-16 14:37:58', 'Мы были восхищены уровнем работы и сроками изготовления. Впечатления о сотрудничестве остались только положительные. Мы были восхищены уровнем работы и сроками изготовления. Впечатления о сотрудничестве остались только положительные. Мы были восхищены уровнем работы и сроками изготовления. ', '1', 'Александра Вострикова', null, null, null, '2', null, '5', '0', 'Владелица загородного дома');
INSERT INTO `yupe_review` VALUES ('5', null, '2020-10-19 14:37:47', 'test', null, 'test', '', null, null, '3', null, '5', null, null);

-- ----------------------------
-- Table structure for `yupe_review_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_review_image`;
CREATE TABLE `yupe_review_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) DEFAULT NULL COMMENT 'Id отзыва',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `alt` varchar(255) DEFAULT NULL COMMENT 'Alt',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`),
  KEY `fk_yupe_review_image_review_id` (`review_id`),
  CONSTRAINT `fk_yupe_review_image_review_id` FOREIGN KEY (`review_id`) REFERENCES `yupe_review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_review_image
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_sitemap_page`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_sitemap_page`;
CREATE TABLE `yupe_sitemap_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL,
  `changefreq` varchar(20) NOT NULL,
  `priority` float NOT NULL DEFAULT '0.5',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_sitemap_page_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_sitemap_page
-- ----------------------------
INSERT INTO `yupe_sitemap_page` VALUES ('1', '/', 'daily', '0.5', '1');

-- ----------------------------
-- Table structure for `yupe_slider`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_slider`;
CREATE TABLE `yupe_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание',
  `description_short` text COMMENT 'Краткое описание',
  `button_name` varchar(255) DEFAULT NULL COMMENT 'Название кнопки',
  `button_link` varchar(255) DEFAULT NULL COMMENT 'url для кнопки',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `image_xs` varchar(250) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image_big` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_slider_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_slider_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_slider_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_slider
-- ----------------------------
INSERT INTO `yupe_slider` VALUES ('1', '<span class=\"line-bottom\">Подчеркните</span>  индивидуальный  дизайн-проект ', null, '3100b009c5ac0e65aceb8dd2c58153da.jpg', null, '', 'Посмотреть порфолио', '#', '1', '1', '', '1', 'b1481d8803b84aa85da9dc5228f7b88f.jpg');
INSERT INTO `yupe_slider` VALUES ('2', '<span class=\"line-bottom\">Подчеркните</span>  индивидуальный  дизайн-проект ', null, '166b3f75dc26a15f1c2abf0e1666ae4e.jpg', null, null, '', '', '1', '2', '', '1', '88ae8dd907916e6ecde88a17d313dee8.jpg');

-- ----------------------------
-- Table structure for `yupe_slider_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_slider_category`;
CREATE TABLE `yupe_slider_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_slider_category
-- ----------------------------
INSERT INTO `yupe_slider_category` VALUES ('1', 'Основной слайд на главной', '1', '1');

-- ----------------------------
-- Table structure for `yupe_subscribe`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_subscribe`;
CREATE TABLE `yupe_subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL COMMENT 'Имя подписчика',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email подписчика',
  `status` int(11) DEFAULT NULL COMMENT 'Статус подписчика',
  `create_time` datetime DEFAULT NULL COMMENT 'Дата создания',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_subscribe_create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_subscribe
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_user_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_tokens`;
CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_tokens
-- ----------------------------
INSERT INTO `yupe_user_tokens` VALUES ('14', '1', 'AexzXHDypdNKHyUuQSZ~Fp3p4K4H_OfQ', '4', '0', '2020-10-29 15:53:33', '2020-10-29 15:53:33', '127.0.0.1', '2020-11-05 15:53:33');

-- ----------------------------
-- Table structure for `yupe_user_user`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user`;
CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '50443789b000cb912ae2c026e7e117600.90330600 1545824979',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user
-- ----------------------------
INSERT INTO `yupe_user_user` VALUES ('1', '2018-12-26 17:50:44', '', '', '', 'admin', 'nariman-abenov@mail.ru', '0', null, '', '', '', '1', '1', '2020-10-29 15:53:32', '2018-12-26 17:50:44', null, '$2y$13$n8zKhyL8z0DR9yUE1HmNlu.QUR6GwqIuebNFXyeHzb2pBqF3DcNci', '1', null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_assignment`;
CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`),
  CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_assignment
-- ----------------------------
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin', '1', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item`;
CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item
-- ----------------------------
INSERT INTO `yupe_user_user_auth_item` VALUES ('admin', '2', 'Admin', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item_child`;
CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`),
  CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item_child
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_yupe_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_yupe_settings`;
CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_yupe_settings
-- ----------------------------
INSERT INTO `yupe_yupe_settings` VALUES ('1', 'yupe', 'siteDescription', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('2', 'yupe', 'siteName', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('3', 'yupe', 'siteKeyWords', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('4', 'yupe', 'email', 'nariman-abenov@mail.ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('5', 'yupe', 'theme', 'default', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('6', 'yupe', 'backendTheme', '', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('7', 'yupe', 'defaultLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('8', 'yupe', 'defaultBackendLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('9', 'homepage', 'mode', '2', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('10', 'homepage', 'target', '1', '2019-01-14 09:39:38', '2019-01-14 09:39:41', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('11', 'homepage', 'limit', '', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('12', 'page', 'editor', 'tinymce5', '2020-05-22 09:50:05', '2020-08-26 11:23:26', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('13', 'page', 'mainCategory', '', '2020-05-22 09:50:05', '2020-05-22 09:50:05', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('14', 'yupe', 'coreCacheTime', '3600', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('15', 'yupe', 'uploadPath', 'uploads', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('16', 'yupe', 'editor', 'tinymce5', '2020-08-26 10:12:15', '2020-10-13 10:28:26', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('17', 'yupe', 'availableLanguages', 'ru,uk,en,zh', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('18', 'yupe', 'allowedIp', '', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('19', 'yupe', 'hidePanelUrls', '0', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('20', 'yupe', 'logo', 'images/logo.png', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('21', 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf, svg', '2020-08-26 10:12:15', '2020-10-18 20:38:53', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('22', 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, image/svg+xml, application/xhtml+xml, application/xml', '2020-08-26 10:12:15', '2020-10-18 20:38:53', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('23', 'yupe', 'maxSize', '5242880', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('24', 'yupe', 'defaultImage', '/images/nophoto.jpg', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('25', 'user', 'avatarMaxSize', '5242880', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('26', 'user', 'avatarExtensions', 'jpg,png,gif,jpeg', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('27', 'user', 'defaultAvatarPath', 'images/avatar.png', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('28', 'user', 'avatarsDir', 'avatars', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('29', 'user', 'showCaptcha', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('30', 'user', 'minCaptchaLength', '3', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('31', 'user', 'maxCaptchaLength', '6', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('32', 'user', 'minPasswordLength', '8', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('33', 'user', 'autoRecoveryPassword', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('34', 'user', 'recoveryDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('35', 'user', 'registrationDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('36', 'user', 'notifyEmailFrom', 'no-reply@dcmr.ru', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('37', 'user', 'logoutSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('38', 'user', 'loginSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('39', 'user', 'accountActivationSuccess', '/user/account/login', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('40', 'user', 'accountActivationFailure', '/user/account/registration', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('41', 'user', 'loginAdminSuccess', '/yupe/backend/index', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('42', 'user', 'registrationSuccess', '/user/account/login', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('43', 'user', 'sessionLifeTime', '7', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('44', 'user', 'usersPerPage', '20', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('45', 'user', 'emailAccountVerification', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('46', 'user', 'badLoginCount', '3', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('47', 'user', 'phoneMask', '+7(999) 999-99-99', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('48', 'user', 'phonePattern', '/\\+7\\(\\d{3}\\) \\d{3}\\-\\d{2}\\-\\d{2}$/', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('49', 'user', 'generateNickName', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('50', 'image', 'uploadPath', 'image', '2020-10-13 10:44:02', '2020-10-13 10:44:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('51', 'image', 'allowedExtensions', 'jpg,jpeg,png,gif,svg', '2020-10-13 10:44:02', '2020-10-18 20:40:48', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('52', 'image', 'minSize', '0', '2020-10-13 10:44:02', '2020-10-13 10:44:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('53', 'image', 'maxSize', '5242880', '2020-10-13 10:44:02', '2020-10-13 10:44:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('54', 'image', 'mainCategory', '', '2020-10-13 10:44:02', '2020-10-13 10:44:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('55', 'image', 'mimeTypes', 'image/gif, image/jpeg, image/png, image/svg+xml, application/xhtml+xml, application/xml', '2020-10-13 10:44:02', '2020-10-18 20:40:48', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('56', 'image', 'width', '1950', '2020-10-13 10:44:02', '2020-10-13 10:44:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('57', 'image', 'height', '1950', '2020-10-13 10:44:02', '2020-10-13 10:44:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('58', 'news', 'editor', '', '2020-10-16 10:54:17', '2020-10-16 10:54:17', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('59', 'news', 'mainCategory', '', '2020-10-16 10:54:17', '2020-10-16 10:54:17', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('60', 'news', 'uploadPath', 'news', '2020-10-16 10:54:17', '2020-10-16 10:54:17', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('61', 'news', 'allowedExtensions', 'jpg,jpeg,png,gif', '2020-10-16 10:54:17', '2020-10-16 10:54:17', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('62', 'news', 'minSize', '0', '2020-10-16 10:54:17', '2020-10-16 10:54:17', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('63', 'news', 'maxSize', '5368709120', '2020-10-16 10:54:18', '2020-10-16 10:54:18', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('64', 'news', 'rssCount', '10', '2020-10-16 10:54:18', '2020-10-16 10:54:18', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('65', 'news', 'perPage', '12', '2020-10-16 10:54:18', '2020-10-19 14:57:54', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('66', 'news', 'metaTitle', '', '2020-10-16 10:54:18', '2020-10-16 10:54:18', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('67', 'news', 'metaDescription', '', '2020-10-16 10:54:18', '2020-10-16 10:54:18', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('68', 'news', 'metaKeyWords', '', '2020-10-16 10:54:18', '2020-10-16 10:54:18', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('69', 'news', 'newsId', '1', '2020-10-16 10:54:18', '2020-10-16 10:54:18', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('70', 'news', 'newsHeadSidebar', 'Читайте также', '2020-10-19 15:22:13', '2020-10-19 15:22:13', '1', '1');
